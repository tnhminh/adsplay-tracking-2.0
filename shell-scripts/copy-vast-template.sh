#!/bin/sh

CODE1="resources/tpl/delivery-ads/vast2.xml.tpl"
CODE2="resources/tpl/delivery-ads/vast3.xml.tpl"
REMOTE_FOLDER="/build/adsplay-server/resources/tpl/delivery-ads"

# Server 50
SERVER50="trieunt@118.69.184.50"
scp -P 228 -r $CODE1 $SERVER50:$REMOTE_FOLDER
scp -P 228 -r $CODE2 $SERVER50:$REMOTE_FOLDER
echo " ===> Copied VAST template $SERVER50 at $(date)"
ssh -p 228 $SERVER50 'sudo bash -s' <  shell-scripts/restart-server-50.sh
echo " Restarted Ok $SERVER50 at $(date) \n"

# Server 46
SERVER46="trieunt@118.69.190.46"
scp -P 228 -r $CODE1 $SERVER46:$REMOTE_FOLDER
scp -P 228 -r $CODE2 $SERVER46:$REMOTE_FOLDER
echo " ===> Copied VAST template $SERVER46 at $(date)"
ssh -p 228 $SERVER46 'sudo bash -s' <  shell-scripts/restart-server-46.sh 
echo " Restarted Ok $SERVER46 at $(date) \n"

# Server 49
SERVER49="trieunt@118.69.184.49"
scp -P 228 -r $CODE1 $SERVER49:$REMOTE_FOLDER
scp -P 228 -r $CODE2 $SERVER49:$REMOTE_FOLDER
echo " ===> Copied VAST template $SERVER49 at $(date)"
ssh -p 228 $SERVER49 'sudo bash -s' <  shell-scripts/restart-server-49.sh 
echo " Restarted Ok $SERVER49 at $(date) \n"

# Server 87
SERVER87="trieunt@42.119.252.87"
scp -P 228 -r $CODE1 $SERVER87:$REMOTE_FOLDER
scp -P 228 -r $CODE2 $SERVER87:$REMOTE_FOLDER
echo " ===> Copied VAST template $SERVER87 at $(date)"
ssh -p 228 $SERVER87 'sudo bash -s' <  shell-scripts/restart-server-87.sh
echo " Restarted Ok $SERVER87 at $(date) \n"

# Server 88
SERVER88="trieunt@42.119.252.88"
scp -P 228 -r $CODE1 $SERVER88:$REMOTE_FOLDER
scp -P 228 -r $CODE2 $SERVER88:$REMOTE_FOLDER
echo " ===> Copied VAST template $SERVER88 at $(date)"
ssh -p 228 $SERVER88 'sudo bash -s' <  shell-scripts/restart-server-88.sh
echo " Restarted Ok $SERVER88 at $(date) \n"

# Server 41
SERVER41="trieunt@118.69.190.41"
scp -P 228 -r $CODE1 $SERVER41:$REMOTE_FOLDER
scp -P 228 -r $CODE2 $SERVER41:$REMOTE_FOLDER
echo " ===> Copied VAST template $SERVER41 at $(date)"
ssh -p 228 $SERVER41 'sudo bash -s' <  shell-scripts/restart-server-41.sh
echo " Restarted Ok $SERVER41 at $(date) \n"

echo "---- DONE ---"