#!/bin/sh

JVM_PARAMS="-Xms512m -Xmx3g -XX:+TieredCompilation -XX:+UseCompressedOops -XX:+DisableExplicitGC -XX:+UseNUMA -XX:MaxPermSize=3g -server -XX:+UseConcMarkSweepGC"
JAR_NAME="adsplay-server-1.0.jar"

/usr/bin/java8 -jar $JVM_PARAMS $JAR_NAME 118.69.184.50 9090  >> /dev/null 2>&1 &
