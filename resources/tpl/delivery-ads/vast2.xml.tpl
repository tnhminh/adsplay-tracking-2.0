<?xml version="1.0" encoding="UTF-8"?>
<VAST version="2.0.1">

	{{#each instreamVideoAds}}
		<Ad id="{{adId}}">
			<InLine>
				<AdSystem version="1.0">AdsPLAY</AdSystem>
				<AdTitle><![CDATA[{{adTitle}}]]></AdTitle>
				
				<Impression><![CDATA[{{logUrl}}?metric=impression&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Impression>
				{{#each impression3rdUrls}}
					<Impression><![CDATA[{{this}}]]></Impression>
				{{/each}}

				<Creatives>
					<Creative sequence="1" id="{{adId}}">						
						<Linear skipoffset="{{skipoffset}}" starttime="{{startTime}}" >
							<Duration>{{duration}}</Duration>
							<TrackingEvents>
								{{trackingEvent3rdTags}}
								<Tracking event="creativeView"><![CDATA[{{logUrl}}?metric=creativeView&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>
								<Tracking event="start"><![CDATA[{{logUrl}}?metric=start&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>							
								<Tracking event="firstQuartile"><![CDATA[{{logUrl}}?metric=view25&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>
								<Tracking event="midpoint"><![CDATA[{{logUrl}}?metric=view50&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>							
								<Tracking event="thirdQuartile"><![CDATA[{{logUrl}}?metric=view75&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>
								<Tracking event="complete"><![CDATA[{{logUrl}}?metric=view100&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>															
								<Tracking event="close"><![CDATA[{{logUrl}}?metric=close&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>							
							</TrackingEvents>
							<VideoClicks>{{videoClicksTags}}<ClickTracking><![CDATA[{{logUrl}}?metric=click&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></ClickTracking></VideoClicks>
							<MediaFiles>							
								<MediaFile height="360" width="640" bitrate="500" type="video/mp4" delivery="progressive"><![CDATA[{{mediaFile}}]]></MediaFile>
							</MediaFiles>
						</Linear>
					</Creative>					
				</Creatives>
			</InLine>
		</Ad>
	{{/each}}		
	
</VAST>
