<?xml version="1.0" encoding="UTF-8"?>
<VAST version="2.0">
   {{#each instreamVideoAds}}
   <Ad id="{{adId}}">
      <Wrapper>
         <AdSystem>AdsPlay-2.0</AdSystem>
         <AdTitle><![CDATA[{{adTitle}}]]></AdTitle>
         <VASTAdTagURI><![CDATA[{{vastAdTagURI}}]]></VASTAdTagURI>
         <Impression><![CDATA[{{logUrl}}?metric=impression&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Impression>
         <Creatives>
            <Creative sequence="1" id="{{adId}}">
               <Linear>
                  <TrackingEvents>
                     <Tracking event="creativeView"><![CDATA[{{logUrl}}?metric=creativeView&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>
                     <Tracking event="start"><![CDATA[{{logUrl}}?metric=start&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>
                     <Tracking event="midpoint"><![CDATA[{{logUrl}}?metric=view50&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>
                     <Tracking event="complete"><![CDATA[{{logUrl}}?metric=view100&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>
                     <Tracking event="close"><![CDATA[{{logUrl}}?metric=close&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></Tracking>
                  </TrackingEvents>
                  <VideoClicks>{{videoClicksTags}}<ClickTracking><![CDATA[{{logUrl}}?metric=click&adid={{adId}}&beacon={{adBeacon}}&t={{time}}]]></ClickTracking></VideoClicks>
               </Linear>
            </Creative>
         </Creatives>
      </Wrapper>
   </Ad>
   {{/each}}
</VAST>