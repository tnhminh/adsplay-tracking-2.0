package adsplay.targeting;

import java.util.HashMap;
import java.util.Map;

public class UserSegmentUtil {
	public static final int GROUP_MALE_AGE_18_TO_36_SPORT = 11836;
	public static final int GROUP_MALE_AGE_18_TO_36_VOD = 21836;
	public static final int UNDEFINED = 0;
	
	static String getLiveChannelName(String url){
		int idx = url.indexOf("/livetv");
		boolean isLiveTV = idx > 0;			
		
		if(isLiveTV){
			String channelName = url.endsWith("/livetv") ? "vtv3" : url.substring(idx + 8);
			int lidx = channelName.indexOf("?");
			if(lidx > 0){
				channelName = channelName.substring(0, lidx);
			}
			lidx = channelName.indexOf("/");
			if(lidx > 0){
				channelName = channelName.substring(0, lidx);
			}
			lidx = channelName.indexOf("#");
			if(lidx > 0){
				channelName = channelName.substring(0, lidx);
			}
			if( ! channelName.contains("#") ){
				return channelName;
			}
		}
		return "";
	}

	public static int getUserSegmentGroup(String uuid, String refererUrl){
		String channelName = getLiveChannelName(refererUrl);	
		if(		"vtv6".equalsIgnoreCase(channelName) 
				|| "vtv6-hd".equalsIgnoreCase(channelName)
				|| "sporttv2".equalsIgnoreCase(channelName)				
				|| "vtv3".equalsIgnoreCase(channelName)
				|| "htv7".equalsIgnoreCase(channelName)
				|| "htvc-the-thao".equalsIgnoreCase(channelName) 
				|| "imovie".equalsIgnoreCase(channelName) 
				){
			return GROUP_MALE_AGE_18_TO_36_SPORT ;
		}		
		return UNDEFINED;
	}
	
	public static boolean isFemaleGroupOnLiveTV(String uuid, String refererHeaderUrl){
		String channelName = getLiveChannelName(refererHeaderUrl);
		if(	"htv3".equalsIgnoreCase(channelName) ||
 			"htv2".equalsIgnoreCase(channelName) || 					
 			"htv7".equalsIgnoreCase(channelName) ||
 			"vtv9".equalsIgnoreCase(channelName) ||
 			"lets-viet".equalsIgnoreCase(channelName)  			
			){
			return true;
		}
		return false;
	}
	
	static Map<String, Boolean> femaleKeyWords = new HashMap<>(50);
	static Map<String, Boolean> maleKeyWords = new HashMap<>(50);
	static Map<String, Boolean> skipAdKeyWords = new HashMap<>(50);
	
	static {
		//phim bo
		femaleKeyWords.put("phim bộ hot", true);
		femaleKeyWords.put("phim bộ hoa ngữ", true);	
		femaleKeyWords.put("phim bộ hàn quốc", true);		
		femaleKeyWords.put("phim bộ việt nam", true);
		femaleKeyWords.put("phim bộ nổi bật", true);		
		femaleKeyWords.put("hàn quốc", true);		
		
		//phim le
		femaleKeyWords.put("tâm lý tình cảm", true);
		femaleKeyWords.put("hài hước", true);
		
		//TV show
		femaleKeyWords.put("tv show hot", true);
		femaleKeyWords.put("tvshow nổi bật", true);
		femaleKeyWords.put("sự kiện", true);		

		maleKeyWords.put("phim lẻ hot", true);
		maleKeyWords.put("phim lẻ", true);
		maleKeyWords.put("viễn tưởng thần thoại", true);		
		maleKeyWords.put("phim lẻ nổi bật, ", true);
		maleKeyWords.put("kinh dị", true);
		maleKeyWords.put("hành động Phiêu Lưu", true);
		maleKeyWords.put("hành động", true);
		maleKeyWords.put("tâm lý", true);
		maleKeyWords.put("bóng đá quốc tế", true);		
		maleKeyWords.put("bản tin", true);
		maleKeyWords.put("bóng đá việt nam", true);
		maleKeyWords.put("tốc độ", true);
		maleKeyWords.put("quần vợt", true);
		maleKeyWords.put("huyền thoại sân cỏ", true);
		maleKeyWords.put("league of legends", true);
		maleKeyWords.put("dota 2", true);
		maleKeyWords.put("thường thức", true);					
	}
	
	public static boolean isSkipByKeywords(String[] keywords){
		try {
			for (String keyword : keywords) {
				if(skipAdKeyWords.getOrDefault(keyword.trim(), false)){
					return true;
				}
			}
		} catch (Exception e) {}		
		return false;
	}
	
	public static boolean isMaleGroupOnVOD(String uuid, String[] keywords){
		try {
			for (String keyword : keywords) {
					if(maleKeyWords.getOrDefault(keyword.trim(), false)){
						System.out.println("isMaleGroupOnVOD keyword "+keyword);
						return true;
					}
			}			
		} catch (Exception e) {}
		return false;
	}
	
	
	public static boolean isFemaleGroupOnVOD(String uuid, String[] keywords){
		try {
			for (String keyword : keywords) {
					if(femaleKeyWords.getOrDefault(keyword.trim(), false)){
						System.out.println("isFemaleGroupOnVOD keyword "+keyword);
						return true;
					}
			}			
		} catch (Exception e) {}
		return false;
	}
	
	public static void main(String[] args) {
		//System.out.println("Trò Chơi Truyền Hình".toLowerCase());
		String uuid = "112";
		String refererUrl = "http://fptplay.net/event/55f7c16a17dc130c3764039b/tinh-cong-tinh.html";
		System.out.println(getLiveChannelName(refererUrl));
		int g = UserSegmentUtil.getUserSegmentGroup(uuid, refererUrl);
		if(g == UserSegmentUtil.GROUP_MALE_AGE_18_TO_36_SPORT){
			System.out.println("show ads");
		}
	}
	
}
