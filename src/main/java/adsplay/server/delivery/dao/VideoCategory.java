package adsplay.server.delivery.dao;

public class VideoCategory {

	String name;
	
	public VideoCategory() {
	}

	public VideoCategory(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
