package adsplay.server.delivery.dao;

import java.io.File;
import java.net.URL;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.io.UrlResource;

import adsplay.common.util.KeywordUtil;

public class SpringContextUtil {
	static String path = "./configs/database-domain.xml";

	static ApplicationContext context = null;

	public static ApplicationContext getContext() {
		if (context == null) {
			try {
				File file = new File(path);
				if (file.isFile()) {
					URL resourceUrl = new URL("file://" + file.getAbsolutePath());
					context = new GenericXmlApplicationContext(new UrlResource(resourceUrl));
				} else {
					System.err.println("Not found Spring config" + path);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return context;
	}



	public static void main(String[] args) {
		VideoCategoryDao dao = getContext().getBean(VideoCategoryDao.class);
		List<VideoCategory> list = dao.getAllVideoCategories();
		for (VideoCategory vcat : list) {
			System.out.println(KeywordUtil.convertUtf8ToAscii(vcat.getName()));
		}
	}
}
