package adsplay.server.delivery.query;

import static adsplay.common.VASTXmlParser.NO_AD_XML;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringEscapeUtils;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import adsplay.common.AdBeaconUtil;
import adsplay.common.template.HandlebarsTemplateUtil;
import adsplay.data.Creative;
import adsplay.server.delivery.query.IPTVDataUtil.IptvAdData;
import adsplay.server.delivery.video.model.AdTrackingUrl;
import rfx.core.util.LogUtil;

public class PayTVXmlGenerator {

	public static final String PAY_TV_XML = "delivery-ads/paytv.xml";

	// Expired time for caching is 6 seconds
	static final int EXPIRED_TIME_CACHING = 6;

	static final LoadingCache<String, String> vastXmlCache = CacheBuilder.newBuilder().maximumSize(2000000)
			.expireAfterWrite(EXPIRED_TIME_CACHING, TimeUnit.SECONDS).build(new CacheLoader<String, String>() {
				@Override
				public String load(String adId) throws Exception {
					return generateXmlContent(Integer.parseInt(adId));
				}
			});

	/**
	 * Cache xml value for query.
	 * 
	 * @param adId
	 * @return xml parsed file
	 */
	public static String getXmlContent(String adId) {
		try {
			return vastXmlCache.get(adId);
		} catch (Exception e) {
			LogUtil.error(e);
		}
		return NO_AD_XML;
	}

	/**
	 * Generate XML Content
	 * @param adId
	 * @return String
	 */
	private static String generateXmlContent(int adId) {
		Query query = new Query();

		// Setting adId
		query.setAdId(adId);

		List<Creative> creatives = AdUnitQueryUtil.queryFromMongoDb(query);
		String cusId = query.getUuid();

		int adID = 0;
		int campaignId = 0;
		int flightId = 0;
		String mp4FileName = null;
		int adsFrequency = 4;

		String skipTime = "5";
		Creative crt = null;
		if (creatives.size() > 0) {
			Collections.shuffle(creatives, new Random(System.nanoTime()));
			crt = creatives.get(0);
			mp4FileName = crt.getMedia();
			adID = crt.getId();
			campaignId = crt.getCampaignId();
			flightId = crt.getFlightId();
			adsFrequency = crt.getFrequencyCapping();
			// get creative skip time
			String skip = crt.getSkipoffset().split(":")[2];
			try {
				skipTime = Integer.valueOf(skip).toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		List<IptvAdData> item = new ArrayList<>(1);
		if (mp4FileName != null) {

			IptvAdData ipTvAd = new IptvAdData(adID, mp4FileName, adsFrequency);

			String adDataParam = AdBeaconUtil.getAdParams(adID, "" + cusId, 320, campaignId, flightId);

			String impTracking = "https://log.adsplay.net/track/videoads?metric=impression" + adDataParam;

			String completeViewTracking = "https://log.adsplay.net/track/videoads?metric=view100" + adDataParam;

			ipTvAd.addStartTrackingUrls(new AdTrackingUrl("start", impTracking, 1));
			ipTvAd.addCompleteTrackingUrl(new AdTrackingUrl("complete", completeViewTracking, 15));

			// set SkipTime
			ipTvAd.setSkipTime(skipTime);
			
			ipTvAd.setDuration(crt.getDuration());

			item.add(ipTvAd);
		}

		String xml = renderXml(PAY_TV_XML, item.get(0));
		System.out.println(xml);
		return xml;
	}

	public static String renderXml(String tplPath, IptvAdData ipTvData) {
		String s = HandlebarsTemplateUtil.execute(tplPath, ipTvData);
		return StringEscapeUtils.unescapeHtml4(s);
	}
}
