package adsplay.server.delivery.query;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.RedisConfigs;
import rfx.core.nosql.jedis.RedisCommand;
import rfx.core.util.StringUtil;

public class SkipAdUtil {
	static LoadingCache<String, Integer> localCache = CacheBuilder.newBuilder()
			.maximumSize(10).expireAfterWrite(5, TimeUnit.SECONDS)
			.build(new CacheLoader<String, Integer>() {
				public Integer load(String key) {
					int n = new RedisCommand<Integer>(jedisPool) {
						@Override
						protected Integer build() throws JedisException {
							String rs = jedis.get(key);				
							if(StringUtil.isEmpty(rs)){
								//default is show ads
								return 5;
							}
							return StringUtil.safeParseInt(rs);
						}
					}.execute();		
					return n;
				}
			});	
	
	static ShardedJedisPool jedisPool = RedisConfigs.load().get("realtimeDataStats").getShardedJedisPool();
	static List<String> skipAdSuffixUrls = new ArrayList<>();
	static List<String> allowSuffixUrls = new ArrayList<>();
	
	static {
		skipAdSuffixUrls.add("f-event");
		skipAdSuffixUrls.add("su-kien-1");
		skipAdSuffixUrls.add("su-kien-2");
		skipAdSuffixUrls.add("v-league-1");
		skipAdSuffixUrls.add("v-league-2");
		skipAdSuffixUrls.add("v-league-3");
		skipAdSuffixUrls.add("v-league-4");
		skipAdSuffixUrls.add("v-league-5");		
		skipAdSuffixUrls.add("liveshow-tran-lap-doi-ban-tay-thap-lua");
		skipAdSuffixUrls.add("xskt-mien-nam-1");
		skipAdSuffixUrls.add("xskt-mien-nam-2");
		skipAdSuffixUrls.add("xskt-mien-bac");
		skipAdSuffixUrls.add("event");
		skipAdSuffixUrls.add("xem-epl");
		
		skipAdSuffixUrls.add("vtv1-hd");
		skipAdSuffixUrls.add("vtv3-hd");
		skipAdSuffixUrls.add("vtv6-hd");
		skipAdSuffixUrls.add("vtv2");
		skipAdSuffixUrls.add("vtv4");
		skipAdSuffixUrls.add("vtv9");
		skipAdSuffixUrls.add("giai-tri-tv");
		skipAdSuffixUrls.add("ddramas");
		skipAdSuffixUrls.add("phim-viet");
		skipAdSuffixUrls.add("bong-da-tv-hd");
		skipAdSuffixUrls.add("the-thao-tv-hd");
		skipAdSuffixUrls.add("bong-da-tv");
		skipAdSuffixUrls.add("the-thao-tv");
		skipAdSuffixUrls.add("info-tv");
		skipAdSuffixUrls.add("invest-tv");
		skipAdSuffixUrls.add("kenh-17");
		skipAdSuffixUrls.add("yeah1");
		skipAdSuffixUrls.add("o2-tv");
		skipAdSuffixUrls.add("lotte-dat-viet-homeshopping");
		
		//Q.net channels
		skipAdSuffixUrls.add("axn");		
		skipAdSuffixUrls.add("warner-tv");
		skipAdSuffixUrls.add("red-by-hbo");
		skipAdSuffixUrls.add("toonami");
		skipAdSuffixUrls.add("cinemaworld");
		skipAdSuffixUrls.add("fashion-tv");
		skipAdSuffixUrls.add("cartoon-network");
		skipAdSuffixUrls.add("cnn");
		skipAdSuffixUrls.add("bloomberg");
		skipAdSuffixUrls.add("australia-plus");
		
		//skip by Content IDs		
				
		
	}
	
	public static boolean isSkipAd(String contextUrl){		
		//System.out.println(contextUrl);
		//filter non-ads url
		if(contextUrl.contains("/livetv") || contextUrl.contains("/event")){
			return true;
		} 
		return false;
		
//		boolean allowed = allowSuffixUrls.parallelStream().filter(pattern -> {
//			if(contextUrl.contains(pattern)){ 
//				return true;				
//			}
//			return false;
//		}).count() > 0;
		
//		boolean allowed = true;
//		if(allowed){
//			return false;
//		} else {			
//			boolean skip = skipAdSuffixUrls.parallelStream().filter(pattern -> {
//				if(contextUrl.contains(pattern)) return true;							
//				return false;
//			}).count() > 0;
//			return skip;
//		}		
	}
	
	public static int getShowAdsLimit(){						
		try {
			return localCache.get("show-ads-limit").intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 1;
	}
	
	public static int getShowAdsLimitScore(){						
		try {
			return localCache.get("show-ads-limit-score").intValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 1;
	}
	
}
