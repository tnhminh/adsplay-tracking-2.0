package adsplay.server.delivery.display.model;

import java.util.List;

import adsplay.common.model.AdDataBaseModel;
import adsplay.server.delivery.video.model.AdTrackingUrl;

public class VideoNativeAdData extends AdDataBaseModel{
	List<AdTrackingUrl> adTracking3rdUrls;
	
	public VideoNativeAdData(int placementId, String adMedia, String clickthroughUrl, String clickActionText, int adId, int adType, List<AdTrackingUrl> adTrackingUrls) {
		super();
		this.adMedia = adMedia;
		this.clickthroughUrl = clickthroughUrl;
		this.clickActionText = clickActionText;
		this.adId = adId;
		this.adType = adType;
		this.adTracking3rdUrls = adTrackingUrls;
	}	
	
	

	public List<AdTrackingUrl> getAdTracking3rdUrls() {
		return adTracking3rdUrls;
	}

	public void setAdTracking3rdUrls(List<AdTrackingUrl> adTracking3rdUrls) {
		this.adTracking3rdUrls = adTracking3rdUrls;
	}
	

}
