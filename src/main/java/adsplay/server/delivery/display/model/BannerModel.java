package adsplay.server.delivery.display.model;

public class BannerModel {

	long timeStamp;
	long adspaceStart;
	long adspaceEnd;
	String tracker;
	String tag;
	String tagUrl;
	String url;	
	String clickTag;
	String poolPath;
	String creativeName;	
	String slotName;
	
	public BannerModel() {
		timeStamp = 1436276762000L;
		tracker = "http://ads.demo.adhese.com/track/69//sl56/ag40/brChrome/brChrome46/brLinux/brdesktop/rn8841/dtdesktop/A2118.69.131.254.1444703276856362/?t=1445252599450";
		tag = "<A TARGET='_blank' HREF='http://www.zalora.vn/Giay-Tennis-Nike-Air-Vapor-Ace-423609.html' TITLE=''><IMG SRC='https://dl.dropboxusercontent.com/u/4074962/adsplay/code/300x250.jpg?t=1436276762000' WIDTH='300' HEIGHT='250' ALT='' BORDER=0/></A>";
		tagUrl = "https://dl.dropboxusercontent.com/u/4074962/adsplay/code/300x250.jpg";
		url = "http://clicks-demo.adhese.com/raylene//sl56/ag40/brChrome/brChrome46/brLinux/brdesktop/rn8841/dtdesktop/A2118.69.131.254.1444703276856362/ad69/URhttp://www.adhese.com";
		clickTag = "http://clicks-demo.adhese.com/raylene//sl56/ag40/brChrome/brChrome46/brLinux/brdesktop/rn8841/dtdesktop/A2118.69.131.254.1444703276856362/ad69/UR";
		poolPath = "http://pool-demo.adhese.com/pool/lib/";
		creativeName = "test";
	    adspaceStart = 1398204000000L;
	    adspaceEnd = 1462053599000L;
	    slotName = "_sdk_example_-imu";
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public String getTracker() {
		return tracker;
	}

	public String getTag() {
		return tag;
	}

	public String getTagUrl() {
		return tagUrl;
	}

	public String getClickTag() {
		return clickTag;
	}

	public String getPoolPath() {
		return poolPath;
	}

	public String getCreativeName() {
		return creativeName;
	}

	public long getAdspaceStart() {
		return adspaceStart;
	}

	public long getAdspaceEnd() {
		return adspaceEnd;
	}

	public String getSlotName() {
		return slotName;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public void setTracker(String tracker) {
		this.tracker = tracker;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public void setTagUrl(String tagUrl) {
		this.tagUrl = tagUrl;
	}

	public void setClickTag(String clickTag) {
		this.clickTag = clickTag;
	}

	public void setPoolPath(String poolPath) {
		this.poolPath = poolPath;
	}

	public void setCreativeName(String creativeName) {
		this.creativeName = creativeName;
	}

	public void setAdspaceStart(long adspaceStart) {
		this.adspaceStart = adspaceStart;
	}

	public void setAdspaceEnd(long adspaceEnd) {
		this.adspaceEnd = adspaceEnd;
	}

	public void setSlotName(String slotName) {
		this.slotName = slotName;
	}
	
	
}
