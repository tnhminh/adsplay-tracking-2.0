package adsplay.server.delivery.display.model;

import java.util.ArrayList;
import java.util.List;

import adsplay.common.model.AdDataBaseModel;

public class BannerAdData extends AdDataBaseModel {

	protected int width = 0;
	protected int height = 0;
	protected List<String> tracking3rdUrls = new ArrayList<>();
	
	public BannerAdData(int placementId, String adMedia, String clickthroughUrl, String clickActionText, int adId, int adType, int width, int height) {
		super();
		this.placementId = placementId;
		this.adMedia = adMedia;
		this.clickthroughUrl = clickthroughUrl;
		this.clickActionText = clickActionText;
		this.adId = adId;
		this.adType = adType;
		this.width = width;
		this.height = height;
	}	

	public BannerAdData(int adType, int width, int height, int placementId, String adCode) {
		super();
		this.adType = adType;
		this.width = width;
		this.height = height;
		this.placementId = placementId;
		this.adCode = adCode;
	}

	public BannerAdData() {
		super();
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}	

	public List<String> getTracking3rdUrls() {
		return tracking3rdUrls;
	}

	public void addTracking3rdUrl(String tracking3rdUrl) {
		this.tracking3rdUrls.add(tracking3rdUrl);
	}
	
	public void addTracking3rdUrls(List<String> tracking3rdUrls) {
		this.tracking3rdUrls.addAll(tracking3rdUrls);
	}

}