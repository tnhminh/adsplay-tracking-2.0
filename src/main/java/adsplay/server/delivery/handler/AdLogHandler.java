package adsplay.server.delivery.handler;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import adsplay.common.PlatformUtil;
import adsplay.common.RealtimeTrackingUtil;
import adsplay.common.util.AdData;
import adsplay.common.util.AdsPlayBeaconUtil;
import adsplay.common.util.BeaconUtil;
import adsplay.common.util.UserRedisUtil;
import adsplay.data.Creative;
import adsplay.server.delivery.query.IPTVDataUtil;
import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.StringUtil;
import server.http.util.HttpTrackingUtil;
import server.http.util.KafkaLogHandlerUtil;

public class AdLogHandler {
	private static final String COMPLETE_ADV = "CompleteAdv";
	public static final String C_PM = "c-pm-";
	public static final String IMP_PM = "imp-pm-";
	public static final String V_C = "v-c";
	public static final String V_I = "v-i";
	public static final String V_TRUEVIEW = "v-trv";
	public static final String V_PLAYVIEW = "v-pv";
	
	public static final String metricImpression = "impression";	
	public static final String metricClick = "click";	
	public static final String metricTrueView = "trueview";		
	public static final String metricPageview = "pageview";
	public static final String metricView50 = "view50";
	public static final String metricView100 = "view100";
	
	//for TVC Ad log
	public static final String logAdsClickKafkaKey = "_ad_click";	
	public static final String logAdsImpressionKafkaKey = "_ad_impression";
	public static final String logAdsTrueViewKafkaKey = "_ad_trueview";
	public static final String logVideoTrackingKafkaKey = "_ad_tracking";
	public static final String logPlayViewKafkaKey = "_playview";	
	
	static Map<Integer,Boolean> view50Ids = new HashMap<>();
	static {
		
	}
	

	public static void handle(HttpServerRequest req, HttpServerResponse resp, MultiMap outHeaders, int placementId){
		MultiMap params = req.params();
		String metric = params.get("metric");
		int adid = StringUtil.safeParseInt(params.get("adid"), 0);	
		if(adid > 0){			
            String beacon = BeaconUtil.getParam(params, "beacon");
            if (StringUtil.isEmpty(beacon)) {
                System.out.println("Beacon is Empty!");
                return;
            }
            int loggedTime = DateTimeUtil.currentUnixTimestamp();
            	                    
			AdData adData = AdsPlayBeaconUtil.parseBeaconData(metric, loggedTime, beacon);			
			if(adData == null){				
				HttpTrackingUtil.trackingResponse(req);
				return;
			}
			int placement = adData.getPlacement();
			int platformId = PlatformUtil.getPlatformId(placement);
			String uuid = adData.getUuid();
			if(metric.equals(metricImpression)){
				KafkaLogHandlerUtil.logAndResponseImage1px(req, logAdsImpressionKafkaKey);
				String k = "impfpm-"+adid+"-"+platformId+"-"+placement;
				String[] events = new String[]{ V_I, V_I+"-"+adid ,	k};				
				RealtimeTrackingUtil.updateEvent(loggedTime, events, true);
				UserRedisUtil.addImpresisonUser(loggedTime, placement, uuid);
				UserRedisUtil.addReachUser(loggedTime, adid, uuid, placement);
			}	
			else if(metric.equals(metricView50)){				
				KafkaLogHandlerUtil.logAndResponseImage1px(req, logVideoTrackingKafkaKey);
				String[] events = new String[] {metricView50, metricView50+"-"+adid};
//				if(view50Ids.containsKey(adid)){
//					//FIXME
//					events = new String[] {metricView100, metricView100+"-"+adid};
//					UserRedisUtil.addTrueViewUser(loggedTime, placement, uuid);
//				} 
				RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), events, true);
			}
			else if(metric.equals(metricView100)){				
				KafkaLogHandlerUtil.logAndResponseImage1px(req, logVideoTrackingKafkaKey);
//				if(!view50Ids.containsKey(adid)){
//					//FIXME
//					String[] events = new String[] {metricView100, metricView100+"-"+adid};
//					RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), events, true);
//					UserRedisUtil.addTrueViewUser(loggedTime, placement, uuid);
//				}
				String[] events = new String[] {metricView100, metricView100+"-"+adid};
				RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), events, true);
				UserRedisUtil.addTrueViewUser(loggedTime, placement, uuid);
			}
			else if(metric.equals(metricClick)){					
				KafkaLogHandlerUtil.logAndResponseImage1px(req, logAdsClickKafkaKey);		
				String[] events = new String[] {V_C, V_C+"-"+adid};
				RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), events, true);
				UserRedisUtil.addClickUser(loggedTime, placement, uuid);
				
//				String ip =  RequestInfoUtil.getRemoteIP(req);
//				StringBuilder log = new StringBuilder();
//				log.append(loggedTime).append(CharPool.TAB);
//				log.append(adid).append(CharPool.TAB);
//				log.append(req.uri()).append(CharPool.TAB);
//				log.append(uuid).append(CharPool.TAB);
//				log.append(platformId).append(CharPool.TAB);
//				log.append(ip);
//				LogUtil.i("", log.toString(), true);
			}						
			else {		
				//other metrics
				KafkaLogHandlerUtil.logAndResponseImage1px(req, logVideoTrackingKafkaKey);
				String[] events = new String[] {metric, metric+"-"+adid};
				RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), events , false);
			}	
		}
		else if(placementId > 0 && metric.equals(metricImpression)){
			String[] events = new String[] {IMP_PM+placementId};
			RealtimeTrackingUtil.updateEvent(DateTimeUtil.currentUnixTimestamp(), events , false);
			HttpTrackingUtil.trackingResponse(req);			
		} else {
			HttpTrackingUtil.trackingResponse(req);	
		}
	}
	
	public static void processLogPayTv(HttpServerRequest req){
		req.bodyHandler(new Handler<Buffer>() {			
			@Override
			public void handle(Buffer event) {
				String rawjson = event.toString();
				int placement = 320;
				try {
					//System.out.println(rawjson);
					JsonParser parser = new JsonParser();
					JsonElement  obj = parser.parse(rawjson);
					if(obj != null){
						JsonObject fieldObj = obj.getAsJsonObject().getAsJsonObject("fields");
						String adEvent = fieldObj.get("Event").getAsString();//CompleteAdv or SkipAdv
						String customerID = fieldObj.get("CustomerID").getAsString();
						String adUrl = fieldObj.get("Url").getAsString();
						
						//TODO						
						String[] tokens = adUrl.split("/");
						//String contractId = jsonObj.get("Contract");
						if(tokens.length > 1){
							int adid = IPTVDataUtil.getAdIdFromAdFile(tokens[tokens.length -1]);
							int loggedTime = DateTimeUtil.currentUnixTimestamp();
													
							if(COMPLETE_ADV.equals(adEvent)){
								String[] events = new String[] {metricView100, metricView100+"-"+adid};
								RealtimeTrackingUtil.updateEvent(loggedTime, events, true);				
								UserRedisUtil.addTrueViewUser(loggedTime, placement, customerID);
							}
							
							int platformId = Creative.PLATFORM_IPTV_OTT;
							String k = StringUtil.join("-","impfpm",adid,platformId,placement);
							String[] events = new String[]{ V_I, V_I+"-"+adid ,	k};				
							RealtimeTrackingUtil.updateEvent(loggedTime, events, true);
							UserRedisUtil.addImpresisonUser(loggedTime, placement, customerID);
							UserRedisUtil.addReachUser(loggedTime, adid, customerID, placement);
						}							
					}
				} catch (Throwable e) {					
					e.printStackTrace();
				}
			}
		});
	}
	
	
	public static void logPlayView(HttpServerRequest req){
		//log to Kafka
		KafkaLogHandlerUtil.log(req,logPlayViewKafkaKey);		

	}
}
