package adsplay.server.delivery.handler;

import adsplay.server.delivery.query.PayTVXmlGenerator;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;

public class PayTVXmlHandler {
	public static void handle(HttpServerRequest req, HttpServerResponse resp) {
		String adId = req.getParam("adId");
		String xmlContent = PayTVXmlGenerator.getXmlContent(adId);
		resp.end(xmlContent);
	}
}
