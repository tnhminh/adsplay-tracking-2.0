package adsplay.server.delivery.handler;

import static adsplay.server.delivery.handler.AdDeliveryHandler.NO_DATA;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import adsplay.api.dao.PlacementMongoDbDao;
import adsplay.common.model.AdDataBaseModel;
import adsplay.data.Creative;
import adsplay.data.Placement;
import adsplay.server.delivery.display.model.BannerAdData;
import adsplay.server.delivery.query.AdUnitQueryUtil;
import adsplay.server.delivery.query.Query;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.stream.util.ua.Client;
import rfx.core.util.RandomUtil;
import rfx.core.util.StringUtil;

public class DisplayAdHandler {
	public static void process(HttpServerRequest req, HttpServerResponse resp, MultiMap outHeaders, MultiMap params,
			Client userAgentClient, int platformId, List<Integer> placementIds, String useragent, String refererHeaderUrl, String origin
			) throws Exception{	
		String uuid = StringUtil.safeString(params.get("uuid"),AdDeliveryHandler.NO_DATA);	
		String location =  StringUtil.safeString(params.get("loc"),NO_DATA);    	
		int delayShowVideo =  StringUtil.safeParseInt(params.get("delay"),5000);
    	outHeaders.set(CONTENT_TYPE, AdDeliveryHandler.APPLICATION_JSON);
    	outHeaders.set(AdDeliveryHandler.ACCESS_CONTROL_ALLOW_ORIGIN, origin );
    	List<AdDataBaseModel> ads = new ArrayList<>(placementIds.size());
    	
		for (int placementId : placementIds) {	
			//first get masthead ad first
			AdDataBaseModel ad = MastheadAdHandler.getAd(uuid, placementId, location,delayShowVideo);
			if(ad == null){
				//then find banner ad
				ad = getDisplayAds(uuid, placementId, location);
			}
			if(ad != null){
				ads.add(ad);
			}
		}
		
    	String json = new Gson().toJson(ads);
		resp.end(json);        
	}
	
	public static AdDataBaseModel getAd(String uuid, int placementId, String location){
		
		System.out.println("placementId "+placementId);
		System.out.println("uuid "+uuid);
		System.out.println("location "+location);		
		
		Query q = new Query(Creative.PLATFORM_PC,placementId);
		q.setUuid(uuid);
		q.setLocation(location);
		q.setAdType(Creative.ADTYPE_IMAGE_DISPLAY_AD);		
		return AdUnitQueryUtil.queryDisplayBannerAdData(q);	
		
	}
	
	public static AdDataBaseModel getDisplayAds(String uuid, int placementId, String location){
		AdDataBaseModel ad = null;		
		Placement plm = PlacementMongoDbDao.get(placementId);
		if(plm == null){
			return null;
		}
		int weight3rd = plm.getWeight3rd();		
		if(weight3rd == 0){
			ad = getAd(uuid, placementId, location);
		} else {
			int d = RandomUtil.getRandom(100)+1;			
			if(d <= weight3rd) {				
				String adCode = plm.getAdCode3rd();
				ad = new BannerAdData(Creative.ADTYPE_BIDDING_AD, plm.getWidth(), plm.getHeight(), placementId, adCode);
			} else {
				ad = getAd(uuid, placementId, location);
			}
		}
		return ad;
	}
	
	
}
