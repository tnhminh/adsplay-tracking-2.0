package adsplay.server.delivery.handler;

import static adsplay.server.delivery.handler.AdDeliveryHandler.ACCESS_CONTROL_ALLOW_ORIGIN;
import static adsplay.server.delivery.handler.AdDeliveryHandler.APPLICATION_JSON;
import static adsplay.server.delivery.handler.AdDeliveryHandler.NO_DATA;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.Gson;

import adsplay.common.Ip2LocationUtil;
import adsplay.common.RealtimeTrackingUtil;
import adsplay.common.RequestInfoUtil;
import adsplay.common.model.StreamAdLogData;
import adsplay.common.template.AdModel;
import adsplay.common.util.UserRedisUtil;
import adsplay.data.Creative;
import adsplay.server.delivery.overlay.model.OverlayAdData;
import adsplay.server.delivery.query.AdUnitQueryUtil;
import adsplay.server.delivery.query.InstreamAdDataUtil;
import adsplay.server.delivery.query.Query;
import adsplay.server.delivery.query.SkipAdUtil;
import adsplay.server.delivery.video.model.AdTrackingUrl;
import adsplay.server.delivery.video.model.InstreamVideoAd;
import adsplay.server.delivery.video.model.VideoAdDataModel;
import adsplay.targeting.LocationDataUtil;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.stream.util.ua.Client;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.LogUtil;
import rfx.core.util.RandomUtil;
import rfx.core.util.StringPool;
import rfx.core.util.StringUtil;

public class OverlayAdHandler {
  private static final int PLACEMENT_OVERLAY_TEXT = 501;

  private static final int PLACEMENT_AUTO_EVENT = 446;

  private static final int PLACEMENT_OVERLAY_BANNER = 401;

  static final int AUTO_HIDE_TIME = 7500;

  public static final String PLAYER_WRAPPER = "player-wrapper";// FIXME
  private static final String ADSPLAY_OVERLAY_DEMO = "adsplay-overlay-demo-";
  static final Set<String> streamUrls = new HashSet<>();
  static {
    streamUrls.add("livetv");
    streamUrls.add("event");
  }

  static String processLogForLiveStreamAds(String url, int platformId, String uuid) {
    // boolean matched = streamUrls.stream().filter(pattern -> { return url.contains(pattern);
    // }).count() > 0;
    int dropoutScore = RandomUtil.getRandom(100) + 1;// 1 to 100
    if (dropoutScore >= 0) {
      Query q = new Query(platformId, PLACEMENT_AUTO_EVENT, uuid, Query.CONTENT_GROUP_VOD);
      AdModel model = AdUnitQueryUtil.queryVideoAd(q);
      VideoAdDataModel videoModel = model.getVideoAdModel();
      List<InstreamVideoAd> instreamVideoAds = videoModel.getInstreamVideoAds();
      int size = instreamVideoAds.size();
      List<StreamAdLogData> ads = new ArrayList<>(size);

      System.out.println("processLogForLiveStreamAds size " + size);
      for (InstreamVideoAd ad : instreamVideoAds) {
        List<AdTrackingUrl> tracking3rdUrls = ad.getStreamAdTrackingUrls();
        System.out.println(tracking3rdUrls);
        ads.add(new StreamAdLogData(tracking3rdUrls));
      }

      // FIXME remove when done
      int j = 1;
      for (int i = 0; i < 8; i++) {
        OverlayAdData overlayAdData = getOverlayAd(Creative.ADTYPE_IMAGE_OVERLAY,
            Creative.PLATFORM_PC, uuid, 401, "", 5000, 40000);
        if (overlayAdData != null) {
          if (overlayAdData.getAdId() == 1370) {
            List<AdTrackingUrl> tracking3rdUrls = overlayAdData.getStreamAdTrackingUrls(j);
            ads.add(new StreamAdLogData(tracking3rdUrls));
            j += 5;
          }
        }
      }


      return (new Gson().toJson(ads));
    }
    return null;
  }

  public static void process(HttpServerRequest req, HttpServerResponse resp, MultiMap outHeaders,
      MultiMap params, Client userAgentClient, int platformId, List<Integer> placementIds,
      String useragent, String refererHeaderUrl, String origin) throws Exception {
    outHeaders.set(CONTENT_TYPE, APPLICATION_JSON);
    outHeaders.set(ACCESS_CONTROL_ALLOW_ORIGIN, origin);

    String url = StringUtil.safeString(params.get("url"), NO_DATA);
    String uuid = StringUtil.safeString(params.get("uuid"), NO_DATA);

    int userType = StringUtil.safeParseInt(StringUtil.safeString(params.get("ut"), NO_DATA));
    boolean showAds = true;
    if (userType == InstreamAdDataUtil.USER_TYPE_VID || uuid.equals("000000")) {
      showAds = false;
    }

    if (url.contains("su-kien-3")) {
      int placement = 101;
      int adid = 1462;
      int loggedTime = DateTimeUtil.currentUnixTimestamp();
      // FIXME
      String k = "impfpm-" + adid + "-" + platformId + "-" + placement;
      String[] events = new String[] {AdLogHandler.V_I, AdLogHandler.V_I + "-" + adid, k};
      RealtimeTrackingUtil.updateEvent(loggedTime, events, true);
      UserRedisUtil.addImpresisonUser(loggedTime, placement, uuid);
      UserRedisUtil.addReachUser(loggedTime, adid, uuid, placement);
    }

    boolean skip = SkipAdUtil.isSkipAd(url);
    System.out.println("processLogForLiveStreamAds skip " + skip);
    if (skip) {
      String json = processLogForLiveStreamAds(url, platformId, uuid);
      if (json != null) {
        resp.end(json);
        return;
      }
    }

    List<OverlayAdData> ads = new ArrayList<>(10);
    if (placementIds.size() > 0 && showAds) {
      String location = StringUtil.safeString(params.get("loc"), NO_DATA);
      String ip = RequestInfoUtil.getRemoteIP(req);

      int d = RandomUtil.getRandom(100) + 1;
      if (d < 30) {
        location = LocationDataUtil.VN_NORTH;
      }

      if (location.equals(NO_DATA)) {
        String cityFromIp = Ip2LocationUtil.findCityAndUpdateStats(ip, location);
        if (LocationDataUtil.northCities.containsKey(cityFromIp)) {
          location = LocationDataUtil.VN_NORTH;
        } else {
          location = LocationDataUtil.VN_SOUTH;
        }
      }

      for (int placementId : placementIds) {
        if (placementId == PLACEMENT_OVERLAY_BANNER) {
          // Overlay Banner Image Ad on Video Player
          int adId = 0;
          int idxDemo = url.indexOf(ADSPLAY_OVERLAY_DEMO);
          if (idxDemo > 0) {
            try {
              adId = StringUtil.safeParseInt(
                  url.substring(idxDemo).replace(ADSPLAY_OVERLAY_DEMO, StringPool.BLANK));
            } catch (Throwable e) {
            }
          } else {
            // FIXME remove hard code TOSHIBA
            // if(url.contains("xem-epl")){
            // adId = 1348;
            // }
          }
          ads.addAll(getOverlayAds(uuid, placementId, location, adId));
          break;
        } else if (placementId == PLACEMENT_AUTO_EVENT) {
          // log data for viral media
          String json = processLogForLiveStreamAds(url, platformId, uuid);
          if (json != null) {
            resp.end(json);
            return;
          }
        } else if (placementId == PLACEMENT_OVERLAY_TEXT) {
          if (url.contains("f-event")) {
            String adtext = "FPT PLAY BOX cùng bạn trên Ravolution Music Festival";
            int adId = 1324;
            OverlayAdData ad = new OverlayAdData(placementId, adtext,
                "https://bit.ly/fptplay-box-buy", "", adId, Creative.ADTYPE_BREAKING_NEWS_OVERLAY,
                10000, 4000, OverlayAdHandler.PLAYER_WRAPPER, 0, 0);
            ads.add(ad);
          }
        }
      }
    }
    String json = new Gson().toJson(ads);
    LogUtil.i("OverlayAdHandler.process", "OverlayAd content: " + json + "\n\n", true);
    resp.end(json);
  }

  // --------------------------------------------------------------------------------


  public static OverlayAdData getOverlayAd(int adType, int platformId, String uuid, int placementId,
      String location, int autoHideTimeout, int timeToShow, int adId) {
    System.out.println("adType " + adType);
    System.out.println("platformId " + platformId);
    System.out.println("placementId " + placementId);
    System.out.println("uuid " + uuid);
    System.out.println("location " + location);

    Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
    q.setAdType(adType);
    if (adId > 0) {
      q.setAdId(adId);
    }
    return AdUnitQueryUtil.queryOverlayAdData(q, autoHideTimeout, timeToShow);
  }

  public static OverlayAdData getOverlayAd(int adType, int platformId, String uuid, int placementId,
      String location, int autoHideTimeout, int timeToShow) {
    return getOverlayAd(adType, platformId, uuid, placementId, location, autoHideTimeout,
        timeToShow, 0);
  }

  static List<OverlayAdData> getOverlayAds(String uuid, int placementId, String location,
      int adId) {
    List<OverlayAdData> ads = new ArrayList<>(2);
    OverlayAdData ad1 = getOverlayAd(Creative.ADTYPE_IMAGE_OVERLAY, Creative.PLATFORM_PC, uuid,
        placementId, location, 10000, 35000, adId);
    if (ad1 != null) {
      ads.add(ad1);
    }
    OverlayAdData ad2 = getOverlayAd(Creative.ADTYPE_IMAGE_OVERLAY, Creative.PLATFORM_PC, uuid,
        placementId, location, 10000, 180000, adId);
    if (ad2 != null) {
      ads.add(ad2);
    }
    OverlayAdData ad3 = getOverlayAd(Creative.ADTYPE_IMAGE_OVERLAY, Creative.PLATFORM_PC, uuid,
        placementId, location, 10000, 600000, adId);
    if (ad3 != null) {
      ads.add(ad3);
    }
    return ads;
  }

  static List<OverlayAdData> getOverlayAds2(String uuid, int placementId, String location,
      int adId) {
    List<OverlayAdData> ads = new ArrayList<>(10);

    int adsFreq = 2;
    int timeShow = 0;
    for (int i = 0; i < adsFreq; i++) {
      if (i == 0) {
        timeShow = 90000;
      } else if (i == 1) {
        timeShow = 150000;
      } else {
        timeShow = timeShow + (120000 * i);
      }

      if (adId > 0) {
        timeShow = 5000;
        OverlayAdData ad = getOverlayAd(Creative.ADTYPE_IMAGE_OVERLAY, Creative.PLATFORM_PC, uuid,
            placementId, location, AUTO_HIDE_TIME, timeShow, adId);
        if (ad != null) {
          ads.add(ad);
        }
      } else {
        OverlayAdData ad1 = getOverlayAd(Creative.ADTYPE_IMAGE_OVERLAY, Creative.PLATFORM_PC, uuid,
            placementId, location, AUTO_HIDE_TIME, timeShow, 0);
        if (ad1 != null) {
          ads.add(ad1);
        }
      }

      OverlayAdData ad1 = getOverlayAd(Creative.ADTYPE_IMAGE_OVERLAY, Creative.PLATFORM_PC, uuid,
          placementId, location, AUTO_HIDE_TIME, timeShow, 0);
      if (ad1 != null) {
        ads.add(ad1);
      }
    }
    return ads;
  }
}
