package adsplay.server.delivery.handler;

import static adsplay.server.delivery.handler.AdDeliveryHandler.NO_DATA;
import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;

import java.util.List;

import com.google.gson.Gson;

import adsplay.common.model.AdDataBaseModel;
import adsplay.common.template.AdModel;
import adsplay.data.AdUnit;
import adsplay.server.delivery.query.AdUnitQueryUtil;
import adsplay.server.delivery.query.Query;
import adsplay.server.delivery.video.model.InstreamVideoAd;
import adsplay.server.delivery.video.model.VideoAdDataModel;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.stream.util.ua.Client;
import rfx.core.util.StringUtil;

public class NativeAdHandler {
  public static void process(HttpServerRequest req, HttpServerResponse resp, MultiMap outHeaders,
      MultiMap params, Client userAgentClient, int platformId, List<Integer> placementIds,
      String useragent, String refererHeaderUrl, String origin) throws Exception {
    String uuid = StringUtil.safeString(params.get("uuid"), AdDeliveryHandler.NO_DATA);
    String location = StringUtil.safeString(params.get("loc"), NO_DATA);
    String contentId = StringUtil.safeString(params.get("ctid"), NO_DATA);
    outHeaders.set(CONTENT_TYPE, AdDeliveryHandler.APPLICATION_JSON);
    outHeaders.set(AdDeliveryHandler.ACCESS_CONTROL_ALLOW_ORIGIN, origin);
    String json = "{}";

    System.out.println(placementIds);
    for (int placementId : placementIds) {
      Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
      q.setContentId(contentId);
      AdModel model = AdUnitQueryUtil.queryVideoAd(q);
      VideoAdDataModel videoModel = model.getVideoAdModel();
      List<InstreamVideoAd> instreamVideoAds = videoModel.getInstreamVideoAds();
      if (instreamVideoAds.size() > 0) {
        json = new Gson().toJson(instreamVideoAds.get(0));
      }
      break;
    }

    resp.end(json);
  }

  public static void servingAd(HttpServerRequest req, HttpServerResponse resp, String uuid,
      int placementId, int adType) {
    Query q = new Query(uuid, placementId);
    // TODO q.setLocation(location);

    AdDataBaseModel ad = null;
    if (adType == AdUnit.ADTYPE_VIDEO_INPAGE_AD) {
      System.out.println("ADTYPE_VIDEO_INPAGE_AD " + new Gson().toJson(q));
      // URL https://d2.adsplay.net/get?uuid=0&placement=1009&adtype=7
      ad = AdUnitQueryUtil.queryInPageVideoAdData(q);
    } else if (adType == AdUnit.ADTYPE_IMAGE_DISPLAY_AD) {
      System.out.println("ADTYPE_IMAGE_DISPLAY_AD" + new Gson().toJson(q));
      q.setAdType(adType);
      // URL https://d2.adsplay.net/get?uuid=0&placement=1008&adtype=6
      ad = AdUnitQueryUtil.queryDisplayBannerAdData(q);
    } else if (adType == 11) {
      resp.end(MastheadAdHandler.getHtml(placementId));
      return;
    } else {
      System.out.println("NOTHING " + new Gson().toJson(q));
    }

    if (ad != null) {
      String json = new Gson().toJson(ad);
      resp.end(json);
    } else {
      resp.end("{}");
    }
  }


}
