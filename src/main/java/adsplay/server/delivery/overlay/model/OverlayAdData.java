package adsplay.server.delivery.overlay.model;

import java.util.ArrayList;
import java.util.List;

import adsplay.common.VASTXmlParser;
import adsplay.server.delivery.display.model.BannerAdData;
import adsplay.server.delivery.video.model.AdTrackingUrl;
import rfx.core.util.DateTimeUtil;

public class OverlayAdData extends BannerAdData {
	String videoPlayerId;
	int autoHideTimeout;
	int timeToShow;
	List<AdTrackingUrl> streamAdTrackingUrls;
		
	public OverlayAdData(int placementId, String adMedia, String clickthroughUrl, String clickActionText, int adId, int adType, int autoHideTimeout, int timeToShow, String videoPlayerId, int width, int height) {
		super();
		this.placementId = placementId; 
		this.adMedia = adMedia;
		this.clickthroughUrl = clickthroughUrl;
		this.clickActionText = clickActionText;
		this.adId = adId;
		this.adType = adType;
		this.videoPlayerId = videoPlayerId;
		this.autoHideTimeout = autoHideTimeout;
		this.timeToShow = timeToShow;
		this.width = width;//480;
		this.height = height;//120;
	}	
	
	public String getVideoPlayerId() {
		return videoPlayerId;
	}

	public int getAutoHideTimeout() {
		return autoHideTimeout;
	}
	
	

	public void setVideoPlayerId(String videoPlayerId) {
		this.videoPlayerId = videoPlayerId;
	}

	public void setAutoHideTimeout(int autoHideTimeout) {
		this.autoHideTimeout = autoHideTimeout;
	}

	public void setTimeToShow(int timeToShow) {
		this.timeToShow = timeToShow;
	}

	public int getTimeToShow() {
		return timeToShow;
	}
		
	
	/**
	 * auto tracking log
	 * 
	 * @return List<AdTrackingUrl>
	 */
	public List<AdTrackingUrl> getStreamAdTrackingUrls(int delay) {	
		if(streamAdTrackingUrls == null){
			streamAdTrackingUrls = new ArrayList<>();
		}		
		for (String url : tracking3rdUrls) {
			streamAdTrackingUrls.add(new AdTrackingUrl(VASTXmlParser.VAST_EVENT_IMPRESSION,url, delay));		
		}		
		String urlImp = "https://log4.adsplay.net/track/videoads?metric=impression&adid="+adId+"&beacon="+adBeacon+"&t="+ DateTimeUtil.currentUnixTimestamp();
		streamAdTrackingUrls.add(new AdTrackingUrl(VASTXmlParser.VAST_EVENT_IMPRESSION,urlImp, delay));
		return streamAdTrackingUrls;
	}
}
