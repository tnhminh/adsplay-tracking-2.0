package adsplay.server.delivery.video.model;

import java.util.ArrayList;
import java.util.List;

public class AdBreak {
  private String timeOffset = "start";
  private String breakType = "linear";
  private String breakId = "preroll";
  private String adTagURI = "";
  List<InstreamVideoAd> instreamVideoAds = new ArrayList<InstreamVideoAd>();

  public AdBreak() {

  }

  public AdBreak(String startTime) {
    switch (startTime) {
      case "00:00:00":
        this.timeOffset = "start";
        this.breakId = "preroll";
        break;
      case "end":
        this.timeOffset = "end";
        this.breakId = "postroll";
        break;
      default:
        this.timeOffset = startTime;
        this.breakId = "midroll";
        break;
    }
  }

  public AdBreak(String timeOffset, String adTagURI) {
    super();
    this.timeOffset = timeOffset;
    this.adTagURI = adTagURI;
    if (!timeOffset.equals("start")) {
      breakId = "midroll-1";
    }
  }

  public String getTimeOffset() {
    return timeOffset;
  }

  public void setTimeOffset(String timeOffset) {
    this.timeOffset = timeOffset;
  }

  public String getBreakType() {
    return breakType;
  }

  public void setBreakType(String breakType) {
    this.breakType = breakType;
  }

  public String getBreakId() {
    return breakId;
  }

  public void setBreakId(String breakId) {
    this.breakId = breakId;
  }

  public String getAdTagURI() {
    return adTagURI;
  }

  public void setAdTagURI(String adTagURI) {
    this.adTagURI = adTagURI;
  }

  public List<InstreamVideoAd> getInstreamVideoAds() {
    return instreamVideoAds;
  }

  public AdBreak addInstreamVideoAd(InstreamVideoAd instreamVideoAd) {
    this.instreamVideoAds.add(instreamVideoAd);
    return this;
  }

}
