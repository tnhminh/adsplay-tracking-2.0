package adsplay.server.delivery.video.model;

import java.util.ArrayList;
import java.util.List;

import adsplay.common.template.DataModel;
import io.netty.handler.codec.http.HttpHeaders;
import rfx.core.util.StringUtil;

public class VideoAdDataModel implements DataModel {

  String id = System.currentTimeMillis() + "";
  List<InstreamVideoAd> instreamVideoAds = new ArrayList<InstreamVideoAd>();
  boolean thirdPartyAd;
  String vastAdTagURI;

  public VideoAdDataModel() {}

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<InstreamVideoAd> getInstreamVideoAds() {
    return instreamVideoAds;
  }


  public VideoAdDataModel addInstreamVideoAd(InstreamVideoAd instreamVideoAd) {
    this.instreamVideoAds.add(instreamVideoAd);
    return this;
  }

  public void setInstreamVideoAds(List<InstreamVideoAd> instreamVideoAds) {
    this.instreamVideoAds = instreamVideoAds;
  }

  public boolean hasAds() {
    return this.instreamVideoAds.size() > 0;
  }

  @Override
  public void freeResource() {
    instreamVideoAds.clear();
  }

  @Override
  public String getClasspath() {
    return VideoAdDataModel.class.getName();
  }

  public boolean isThirdPartyAd() {
    return thirdPartyAd;
  }

  public void setThirdPartyAd(boolean thirdPartyAd) {
    this.thirdPartyAd = thirdPartyAd;
  }

  public String getVastAdTagURI() {
    return vastAdTagURI;
  }

  public void setVastAdTagURI(String vastAdTagURI) {
    if (StringUtil.isNotEmpty(vastAdTagURI)) {
      this.vastAdTagURI = vastAdTagURI;
      this.thirdPartyAd = true;
    }
  }

  @Override
  public boolean isOutputable() {
    return true;
  }

  @Override
  public List<HttpHeaders> getHttpHeaders() {
    return new ArrayList<HttpHeaders>(0);
  }
}
