package adsplay.server.delivery.video.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import adsplay.common.template.DataModel;
import io.netty.handler.codec.http.HttpHeaders;

public class VmapDataModel implements DataModel {

  String id = System.currentTimeMillis() + "";
  Set<AdBreak> adBreaks = new HashSet<AdBreak>();

  public VmapDataModel() {

  }

  public String getId() {
    return id;
  }

  public void addAdBreak(AdBreak adBreak) {
    this.adBreaks.add(adBreak);
  }

  public Set<AdBreak> getAdBreaks() {
    return adBreaks;
  }

  public boolean hasAds() {
    return this.adBreaks.size() > 0;
  }

  @Override
  public void freeResource() {
    this.adBreaks.clear();
  }

  @Override
  public String getClasspath() {
    return VmapDataModel.class.getName();
  }

  @Override
  public boolean isOutputable() {
    return true;
  }

  @Override
  public List<HttpHeaders> getHttpHeaders() {
    return new ArrayList<HttpHeaders>(0);
  }
}
