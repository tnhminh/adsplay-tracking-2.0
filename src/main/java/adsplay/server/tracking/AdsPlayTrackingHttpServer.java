package adsplay.server.tracking;

import rfx.core.util.StringUtil;

public class AdsPlayTrackingHttpServer {
    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            args = new String[] { "127.0.0.1", "9922" };
        }
        String host = args[0];
        int port = StringUtil.safeParseInt(args[1]);
        System.setProperty("vertx.disableFileCPResolving", "true");

        // Init redis cache first
        // DeliveryRedisCache.updateCachingTask();

        // Adding new Instance here
        // AdDeliveryWorker.startNewInstance(host, port, new RoutingHandler());
        TrackingWorker.startNewInstance(host, port, new TrackingRoutingHandler());
    }
}
