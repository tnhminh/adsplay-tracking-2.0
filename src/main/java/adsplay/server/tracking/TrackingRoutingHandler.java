package adsplay.server.tracking;

import static io.vertx.core.http.HttpHeaders.CONNECTION;
import static io.vertx.core.http.HttpHeaders.CONTENT_LENGTH;
import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpStatus;

import adsplay.common.BaseHttpLogHandler;
import adsplay.common.util.DateTimeUtil;
import adsplay.common.util.ParserUtils;
import adsplay.common.util.RedisUtils;
import adsplay.delivery.redis.cache.RealtimeTracking;
import adsplay.delivery.redis.cache.TrackingRedisCache;
import adsplay.delivery.redis.cache.TrackingRedisPool;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import rfx.core.configs.WorkerConfigs;
import rfx.core.util.StringUtil;

public class TrackingRoutingHandler extends BaseHttpLogHandler {

    private static final String INV = "inv";
    private static final String PLACEMENT_PREFIX = "placement_";
    private static final String CONTENT_TO_CATEGORIES = "ctc_";
    private static final String CONTENT_PREFIX = "content_";
    private static final String RP = "rp";
    private static final String DATE = "date";
    private static final String DATEHOUR = "datehour";
    private static final String REQUEST_INV = "request";
    public static final String ADSPLAY_NET = "adsplay.net";
    public static final String APLUUID = "apluuid";

    public static final String PREFIX_VIDEO_LOG = "/track/videoads";

    public static final String POWERED_BY = "PoweredBy";
    public static final String ADS_PLAY_VERSION = "AdsPlayServer-2.1";
    public static final String HTTP = "http://";
    private static final WorkerConfigs WORKER_CONFIGS = WorkerConfigs.load();

    public static final String BASE_DOMAIN = WORKER_CONFIGS.getCustomConfig("baseServerDomain");
    public static final String LOG1_DOMAIN = WORKER_CONFIGS.getCustomConfig("log1ServerDomain");
    public static final String LOG2_DOMAIN = WORKER_CONFIGS.getCustomConfig("log2ServerDomain");
    public static final String LOG3_DOMAIN = WORKER_CONFIGS.getCustomConfig("log3ServerDomain");
    public static final String LOG4_DOMAIN = WORKER_CONFIGS.getCustomConfig("log4ServerDomain");

    private static final List<String> BOOKING_EVENTS = new ArrayList<String>();
    static {
        BOOKING_EVENTS.add("impression");
        BOOKING_EVENTS.add("view100");
        BOOKING_EVENTS.add("click");
    }

    private static final List<String> INV_EVENTS = new ArrayList<String>();
    static {
        INV_EVENTS.add(REQUEST_INV);
        INV_EVENTS.add("impression");
    }

    private static final List<String> RQ_EVENTS = new ArrayList<String>();
    static {
        RQ_EVENTS.add("impression");
        RQ_EVENTS.add("click");
        RQ_EVENTS.add("start");
        RQ_EVENTS.add("view25");
        RQ_EVENTS.add("view50");
        RQ_EVENTS.add("view75");
        RQ_EVENTS.add("view100");
        RQ_EVENTS.add("close");
        RQ_EVENTS.add("creativeView");
        RQ_EVENTS.add("mute");
        RQ_EVENTS.add("unmute");

    }

    public TrackingRoutingHandler() {
    }

    @Override
    public void handle(HttpServerRequest request) {
    }

    public static void runWorker(String uri, MultiMap params, List<String> pmIds, Future<String> future) {
        // Do the blocking operation in here
        String rsText = "";
        if (uri.contains("/favicon.ico")) {
            return;
        }

        // Video Tracking
        if (uri.startsWith(PREFIX_VIDEO_LOG)) {
            String eventName = params.get("event");
            int placementId = StringUtil.safeParseInt(params.get("placementId"), 0);
            int adid = StringUtil.safeParseInt(params.get("adId"), 0);
            int flightId = StringUtil.safeParseInt(params.get("flightId"), 0);
            int clientTime = StringUtil.safeParseInt(params.get("t"), 0);
            int campaignId = StringUtil.safeParseInt(params.get("campaignId"), 0);
            String catid = StringUtil.safeString(params.get("catId"), "");
            String buyType = StringUtil.safeString(params.get("buyType"), "");
            int sourceProvider = StringUtil.safeParseInt(params.get("sourceProvider"), 0);
            String contentId = StringUtil.safeString(params.get("contentId"), "");

            updateRealtimeBookingRedis(campaignId, flightId, adid, eventName, placementId, catid, buyType, sourceProvider, contentId);
        }

        future.complete(rsText);
    }

    private static void updateRealtimeBookingRedis(int campaignId, int flightId, int adId, String event, int placementId, String catId,
            String buyType, int sourceProvider, String contentId) {

        // Create booking type key
        String bkTypeKey = RedisUtils.BK + RedisUtils.UNDERLINE + flightId + RedisUtils.UNDERLINE + RedisUtils.BOOKING;
        // Get booking value from redis cache
        String bkTypeVal = StringUtil.safeString(RealtimeTracking.get(bkTypeKey), "impression");

        String dailyByDate = DateTimeUtil.getCurrentDateString("UTC", DateTimeUtil.YYYYMMDD, true, 0, 0, 0);

        String dailyByHour = DateTimeUtil.getCurrentDateString("UTC", DateTimeUtil.YYYYMMDDHH, true, 0, 0, 0);

        updateBookingData(flightId, event, bkTypeVal, dailyByDate);

        boolean [] isRequestEvent = new boolean[1];
        
        updateInventoryData(event, placementId, dailyByDate, dailyByHour, isRequestEvent);

        updateReportData(campaignId, flightId, adId, event, placementId, buyType, sourceProvider, contentId, dailyByDate, dailyByHour,
                isRequestEvent);
       
        // Adding rp_placements tracking
        updatePlacementData(placementId, dailyByDate);
    }

    private static void updatePlacementData(int placementId, String dailyByDate) {
        Map<String, List<String>> pmEvents = new HashMap<String, List<String>>();
        List<String> rpPlacementEv = new ArrayList<String>();
        rpPlacementEv.add(StringUtil.safeString(placementId, "0"));
        pmEvents.put("rp_placements" + "_" + dailyByDate, rpPlacementEv);

        RealtimeTracking.updateHEvent(pmEvents, TrackingRedisPool.PLACEMENT);
    }

    private static void updateReportData(int campaignId, int flightId, int adId, String event, int placementId, String buyType, int sourceProvider,
            String contentId, String dailyByDate, String dailyByHour, boolean[] isRequestEvent) {
        // get categories here
        String structureIds = TrackingRedisCache.get(CONTENT_TO_CATEGORIES + contentId, TrackingRedisPool.API_DATA);

        String[] catArr = ParserUtils.convertStringToArray(structureIds);

        // get local content
        String localContentId = StringUtil.safeString(TrackingRedisCache.get(CONTENT_PREFIX + contentId, TrackingRedisPool.API_DATA), "no-contentId");

        List<String> rpDailyHourCats = new ArrayList<String>();
        if (catArr.length == 0){
            String rpDailyHourlyValCats = campaignId + "_" + flightId + "_" + adId + "_" + sourceProvider + "_" + localContentId + "_" + "no-categories-founded"
                    + "_" + buyType;
            rpDailyHourCats.add(rpDailyHourlyValCats);
        } else {
            int i = 0;
            while (i < catArr.length) {
                String rpDailyHourlyValCats = campaignId + "_" + flightId + "_" + adId + "_" + sourceProvider + "_" + localContentId + "_" + catArr[i]
                        + "_" + buyType;
                rpDailyHourCats.add(rpDailyHourlyValCats);
                i++;
            }
        }
        
        String rpDailyHourlyVal = campaignId + "_" + flightId + "_" + adId + "_" + sourceProvider + "_" + localContentId + "_" + buyType;
        rpDailyHourCats.add(rpDailyHourlyVal);

        if (!isRequestEvent[0]){
            String rpDateKey = RP + "_" + placementId + "_" + DATE + "_" + dailyByDate + "_" + event;

            String rpHourKey = RP + "_" + placementId + "_" + DATEHOUR + "_" + dailyByHour + "_" + event;
            
            Map<String, List<String>> rpEvents = new HashMap<String, List<String>>();
            rpEvents.put(rpDateKey, rpDailyHourCats);
            rpEvents.put(rpHourKey, rpDailyHourCats);
            
            // Update report tracking - hincr
            RealtimeTracking.updateHEvent(rpEvents, TrackingRedisPool.REPORT_DATA);
        }
    }

    private static void updateInventoryData(String event, int placementId, String dailyByDate, String dailyByHour, boolean[] isRequestEvent) {
        Map<String, List<String>> invEvents = null;
        if (INV_EVENTS.contains(event)) {
            
            if (REQUEST_INV.equals(event)){
                isRequestEvent[0] = true;
            }
            
            List<String> invVals = new ArrayList<String>();
            String invDateKey = INV + "_" + placementId + "_" + DATE + "_" + dailyByDate + "_" + event;
            String invDateHourKey = INV + "_" + placementId + "_" + DATEHOUR + "_" + dailyByHour + "_" + event;

            // Get ad_view from placement in redis cache
            String adView = StringUtil.safeString(TrackingRedisCache.hget(PLACEMENT_PREFIX + placementId, TrackingRedisPool.PLACEMENT).get("ad_view"), "no-ad-view");
            invVals.add(adView);

            invEvents = new HashMap<String, List<String>>();
            invEvents.put(invDateKey, invVals);
            invEvents.put(invDateHourKey, invVals);
            
            // Update inventory event
            RealtimeTracking.updateHEvent(invEvents, TrackingRedisPool.INV_DATA);
        }
    }

    private static void updateBookingData(int flightId, String event, String bkTypeVal, String dailyByDate) {
        String[] bkEvents = new String[0];
        String totalBookingKey = "";
        String dailyBookingKey = "";

        if (!bkTypeVal.isEmpty() && BOOKING_EVENTS.contains(event)) {
            totalBookingKey = RedisUtils.BK + RedisUtils.UNDERLINE + flightId + RedisUtils.UNDERLINE + RedisUtils.TOTAL
                    + RedisUtils.UNDERLINE + bkTypeVal;
            dailyBookingKey = RedisUtils.BK + RedisUtils.UNDERLINE + flightId + RedisUtils.UNDERLINE + RedisUtils.DAILY
                    + RedisUtils.UNDERLINE + dailyByDate + RedisUtils.UNDERLINE + bkTypeVal;
            bkEvents = new String[] { totalBookingKey, dailyBookingKey };
            
            // Update total/daily booking in redis
            RealtimeTracking.updateEvent(bkEvents, true, TrackingRedisPool.BOOKING);
        }
    }

    public static void routingToHandler(HttpServerRequest request, Vertx vertxInstance) {
        try {
            String uri = request.uri();
            System.out.println("RoutingHandler.URI: " + uri);
            MultiMap params = request.params();

            List<String> pmIds = params.getAll("placementId");

            // Adding executeBlocking here
            vertxInstance.<String>executeBlocking(future -> {
                TrackingRoutingHandler.runWorker(uri, params, pmIds, future);
            }, res -> {
                if (res.succeeded()) {
                    doResponse(uri, res.result(), pmIds, request);
                } else {
                    res.cause().printStackTrace();
                }
            });

        } catch (Throwable e) {
            String err = e.getMessage();
            request.response().setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR).end(err);
        }
    }

    public static final String GIF = "image/gif";
    public static final String HEADER_CONNECTION_CLOSE = "Close";
    public static final String BASE64_GIF_BLANK = "R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";

    public static void doResponse(String uri, String rsText, List<String> pmIds, HttpServerRequest req) {

        Buffer buffer = Buffer.buffer(BASE64_GIF_BLANK);
        HttpServerResponse response = req.response();
        MultiMap headers = response.headers();
        headers.set(CONTENT_TYPE, GIF);
        headers.set(CONTENT_LENGTH, String.valueOf(buffer.length()));
        headers.set(CONNECTION, HEADER_CONNECTION_CLOSE);
        setCorsHeaders(headers);
        response.end(buffer);

    }

    private final static void setCorsHeaders(MultiMap headers) {
        headers.set("Access-Control-Allow-Origin", "*");
        headers.set("Access-Control-Allow-Credentials", "true");
        headers.set("Access-Control-Allow-Methods", "POST, GET");
        headers.set("Access-Control-Allow-Headers", "origin, content-type, accept, Set-Cookie");
    }

}
