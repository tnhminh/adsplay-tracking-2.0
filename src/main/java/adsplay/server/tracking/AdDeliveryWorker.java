package adsplay.server.tracking;

import org.apache.http.HttpStatus;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import rfx.core.stream.node.worker.BaseWorker;
import server.http.handler.BaseHttpHandler;

public class AdDeliveryWorker extends BaseWorker {
	
	final BaseHttpHandler httpHandler;
	static AdDeliveryWorker instance;

	protected AdDeliveryWorker(String name, BaseHttpHandler httpHandler) {
		super(name);
		this.httpHandler = httpHandler; 
	}
	
	static String getName(String host, int port){
		return AdDeliveryWorker.class.getSimpleName() + "_" + host + "_" + port;
	}
	
	/**
	 * Create new AdDeliveryWorker instance with implemented httpHandler
	 * 
	 * @param host
	 * @param port
	 * @param httpHandler
	 */
	public static void startNewInstance(String host, int port,BaseHttpHandler httpHandler) {
		instance = new AdDeliveryWorker(getName(host, port), httpHandler );
		instance.start(host, port);
	}	
	
	@Override
	protected void onStartDone() {
		System.out.println("AdDeliveryWorker is loaded ...");
	}
	 
	public static AdDeliveryWorker getInstance() {
		if(instance == null){
			throw new IllegalAccessError("startNewInstance must called before getInstance");
		}
		return instance;
	}	

	@Override
	public void start(String host, int port) {
		registerWorkerHttpHandler(host, port, new Handler<HttpServerRequest>() {
			@Override
			public void handle(HttpServerRequest req) {
				try {
					httpHandler.handle(req);
				} catch (Throwable e) {
					String err = e.getMessage();
					req.response().setStatusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR).end(err);
				}
			}
		});		
	}

}
