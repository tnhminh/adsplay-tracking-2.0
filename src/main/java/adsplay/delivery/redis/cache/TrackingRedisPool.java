package adsplay.delivery.redis.cache;

public enum TrackingRedisPool {
    BOOKING, CREATIVE, FLIGHT, PLACEMENT, API_DATA, REPORT_DATA, INV_DATA
}
