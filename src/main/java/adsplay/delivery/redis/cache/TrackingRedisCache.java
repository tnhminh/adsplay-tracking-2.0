package adsplay.delivery.redis.cache;

import java.util.HashMap;
import java.util.Map;

import redis.clients.jedis.Pipeline;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.RedisConfigs;
import rfx.core.nosql.jedis.RedisCommand;

public class TrackingRedisCache {

    private static final String API_DATA = "apiData";

    private static final String FLIGHT_DATA = "flightData";

    private static final String CREATIVE_DATA = "creativeData";

    private static final String PLACEMENT_DATA = "placementData";

    private static final String BOOKING_DATA = "bookingData";

    // Get redis pool in redis.config, need to share in all separated port
    static ShardedJedisPool bookingPool = RedisConfigs.load().get(BOOKING_DATA).getShardedJedisPool();

    static ShardedJedisPool placementPool = RedisConfigs.load().get(PLACEMENT_DATA).getShardedJedisPool();

    static ShardedJedisPool creativeData = RedisConfigs.load().get(CREATIVE_DATA).getShardedJedisPool();

    static ShardedJedisPool flightData = RedisConfigs.load().get(FLIGHT_DATA).getShardedJedisPool();
    
    static ShardedJedisPool apiDataPool = RedisConfigs.load().get(API_DATA).getShardedJedisPool();

    public static Map<String, String> hget(final String key, TrackingRedisPool redisPool) {
        
        ShardedJedisPool redisPoolProxy = null;

        if (redisPool.equals(TrackingRedisPool.BOOKING)) {
            redisPoolProxy = bookingPool;
        } else if (redisPool.equals(TrackingRedisPool.CREATIVE)) {
            redisPoolProxy = creativeData;
        } else if (redisPool.equals(TrackingRedisPool.FLIGHT)) {
            redisPoolProxy = flightData;
        } else if (redisPool.equals(TrackingRedisPool.PLACEMENT)) {
            redisPoolProxy = placementPool;
        }else if (redisPool.equals(TrackingRedisPool.API_DATA)) {
            redisPoolProxy = apiDataPool;
        }
        Map<String, String> result = new RedisCommand<Map<String, String>>(redisPoolProxy) {
            @Override
            protected Map<String, String> build() throws JedisException {
                return jedis.hgetAll(key);
            }
        }.execute();
        return result;
    }

    public static String get(final String key, TrackingRedisPool poolName) {
        String result = "";
        ShardedJedisPool redisPoolProxy = null;

        if (poolName.equals(TrackingRedisPool.BOOKING)) {
            redisPoolProxy = bookingPool;
        } else if (poolName.equals(TrackingRedisPool.CREATIVE)) {
            redisPoolProxy = creativeData;
        } else if (poolName.equals(TrackingRedisPool.FLIGHT)) {
            redisPoolProxy = flightData;
        } else if (poolName.equals(TrackingRedisPool.PLACEMENT)) {
            redisPoolProxy = placementPool;
        } else if (poolName.equals(TrackingRedisPool.API_DATA)) {
            redisPoolProxy = apiDataPool;
        }

        result = new RedisCommand<String>(redisPoolProxy) {
            @Override
            protected String build() throws JedisException {
                return jedis.get(key);
            }
        }.execute();
        return result;
    }

    public static boolean insertDummyData() {
        boolean commited = false;
        try {
            RedisCommand<Boolean> cmd = new RedisCommand<Boolean>(creativeData) {
                @Override
                protected Boolean build() throws JedisException {
                    Pipeline p = jedis.pipelined();
                    Map<String, String> startTimeMap = new HashMap<String, String>();
                    startTimeMap.put("type", "mid_roll");
                    startTimeMap.put("value", "00:00:10");

                    Map<String, String> creativeMap = new HashMap<String, String>();
                    creativeMap.put("source", "http://cdn.adsplay.net/2018/01/video.mp4");
                    creativeMap.put("landing_page", "https://www.vinamilk.com.vn");
                    creativeMap.put("skip_time", "00:00:05");
                    creativeMap.put("duration", "00:00:30");
                    creativeMap.put("flightID", "20");
                    creativeMap.put("start_time", startTimeMap.toString());
                    p.hmset("creative_12", creativeMap);

                    p.sync();
                    return true;
                }
            };
            if (cmd != null) {
                commited = cmd.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commited;
    }

    public static void main(String[] args) {
        // insertDummyData();
        // Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        // gson.toJson(getCacheByCache("creative_12"));
    }
}
