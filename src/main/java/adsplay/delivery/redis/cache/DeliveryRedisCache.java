package adsplay.delivery.redis.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import adsplay.data.Creative;
import adsplay.server.delivery.query.AdUnitQueryUtil;
import adsplay.server.delivery.query.IPTVDataUtil;
import adsplay.server.delivery.query.Query;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.RedisConfigs;
import rfx.core.nosql.jedis.RedisCommand;
import rfx.core.util.LogUtil;

public class DeliveryRedisCache {

    public static final int INT_DEFAULT = 0;

    public static final String STR_DEFAULT = "X";

    private static final String DELIVERY_CACHE = "deliveryCache";

    private static ShardedJedisPool deliveryCache = RedisConfigs.load().get(DELIVERY_CACHE).getShardedJedisPool();

    private static Map<String, Set<Integer>> adIdQueryPairRedisCache = new HashMap<String, Set<Integer>>();

    private static Map<Integer, Creative> creativeCaches = new HashMap<Integer, Creative>();

    public static final int ONE_MINUTE = 60;

    public static final int AFTER_2_MINUTES = ONE_MINUTE * 2;

    private static Timer timer = new Timer();

//    static {
//        updateCachingTask();
//    }

    public static void updateCachingTask() {
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                try {
                    long startTime = System.currentTimeMillis();
                    initialRedisCaching();
                    long duration = System.currentTimeMillis() - startTime;
                    System.out.println("initialRedisCaching duration:" + duration);
                    LogUtil.i("updateCachingTask run ...", "duration time: " + duration, true);
                } catch (Exception e) {
                    LogUtil.error(e);
                }
            }

        }, 1, 120000);
    }

    private static void initialRedisCaching() {
        // 1. Query from mongoDB
        List<Creative> creatives = queryFromMongoDB();
        // 2. Do groupby to key value
        Map<Integer, List<String>> groupByResult = doGroupingBy(creatives);
        System.out.println(groupByResult.size());
        // 3. Mapping for redis cache
        mappingForRedisCache(groupByResult);
        // 4. Update in redis cache
        // updateEvent("paytv-api");
    }

    private static void mappingForRedisCache(Map<Integer, List<String>> groupByResult) {
        adIdQueryPairRedisCache.clear();
        for (Entry<Integer, List<String>> entry : groupByResult.entrySet()) {
            Integer key = entry.getKey();
            for (String s : entry.getValue()) {
                Set<Integer> adIds = adIdQueryPairRedisCache.get(s);
                if (adIds == null) {
                    adIds = new HashSet<Integer>();
                    adIdQueryPairRedisCache.put(s, adIds);
                }
                adIds.add(key);
            }
        }
        LogUtil.i("mappingForRedisCache", "adIdQueryPairRedisCache size:" + adIdQueryPairRedisCache.size(), true);
        System.out.println(adIdQueryPairRedisCache.size());
    }

    public static List<Creative> get(String api) {
        List<Creative> result = new ArrayList<Creative>();
        
        LogUtil.i("adIdQueryPairRedisCache cache", "size: " + adIdQueryPairRedisCache.size(), true);
        Set<Integer> set = adIdQueryPairRedisCache.get(api);
        try {
            if (api != null && set != null) {
                for (Integer adId : set) {
                    result.add(creativeCaches.get(adId));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.e("Check null adIdQuery", "adIdQueryPairRedisCache is:" + adIdQueryPairRedisCache + ", adIdQueryPairRedisCache.get(api) is" + set);
            LogUtil.error(e);
        }
        return result;
    }

    private static Map<Integer, List<String>> doGroupingBy(List<Creative> creatives) {
        Map<Integer, List<String>> adIdQueryPair = new HashMap<Integer, List<String>>();
        for (Creative crt : creatives) {
            List<String> apiStrings = new ArrayList<String>();
            Integer adId = crt.getId();

            List<Integer> targetedContentCategories = crt.getTargetedContentCategories();
            if (targetedContentCategories == null || targetedContentCategories.isEmpty()) {
                targetedContentCategories = new ArrayList<Integer>();
                targetedContentCategories.add(INT_DEFAULT);
            }
            for (Integer catId : targetedContentCategories) {
                List<String> targetedLocations = crt.getTargetedLocations();
                if (targetedLocations == null || targetedLocations.isEmpty()) {
                    targetedLocations = new ArrayList<String>();
                    targetedLocations.add(STR_DEFAULT);
                }
                for (String locStr : targetedLocations) {
                    List<Integer> targetCopyrights = crt.getTargetCopyrights();
                    if (targetCopyrights == null || targetCopyrights.isEmpty()) {
                        targetCopyrights = new ArrayList<Integer>();
                        targetCopyrights.add(INT_DEFAULT);
                    }
                    for (Integer cprId : targetCopyrights) {
                        List<Integer> targetMovies = crt.getTargetMovies();
                        if (targetMovies == null || targetMovies.isEmpty()) {
                            targetMovies = new ArrayList<Integer>();
                            targetMovies.add(INT_DEFAULT);
                        }
                        for (Integer mvId : targetMovies) {
                            StringBuilder idGroup = new StringBuilder();
                            idGroup.append(catId).append("-");
                            idGroup.append(locStr).append("-");
                            idGroup.append(cprId).append("-");
                            idGroup.append(mvId);
                            apiStrings.add(idGroup.toString());
                            //System.out.println(idGroup.toString() + ", id: " + adId);
                        }
                    }
                }
            }
            adIdQueryPair.put(adId, apiStrings);
            //System.out.println("apiStrings: " + apiStrings.size() + ", id:" + adId);
        }
        return adIdQueryPair;
    }

    public static boolean updateEvent(String prefix) {
        boolean commited = false;
        try {
            RedisCommand<Boolean> cmd = new RedisCommand<Boolean>(deliveryCache) {
                @Override
                protected Boolean build() throws JedisException {
                    Pipeline p = jedis.pipelined();
                    for (Entry<String, Set<Integer>> event : adIdQueryPairRedisCache.entrySet()) {
                        String keyH = prefix + ":" + event.getKey();
                        // hourly
                        p.hincrBy(keyH, event.getValue().toString(), 1L);
                        p.expire(keyH, AFTER_2_MINUTES);
                    }

                    p.sync();
                    return true;
                }
            };
            if (cmd != null) {
                commited = cmd.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        LogUtil.i("updateEvent delivery cache", "commited is:" + commited, true);
        return commited;
    }

    private static List<Creative> queryFromMongoDB() {
        long startTime = System.currentTimeMillis();
        Query query = new Query(IPTVDataUtil.platformId, IPTVDataUtil.placementId, "", Query.CONTENT_GROUP_VOD, "", 0);
        List<Creative> creatives = AdUnitQueryUtil.queryFromMongoDb(query, false);
        long duration = System.currentTimeMillis() - startTime;
        LogUtil.i("queryFromMongoDB", "creatives.size:" + creatives.size() + ", took " + duration + " (ms)", true);

        creativeCaches.clear();
        creatives.forEach(crt -> {
            creativeCaches.put(crt.getId(), crt);
        });
        return creatives;
    }

    public static void main(String[] args) {
        initialRedisCaching();
    }
}
