package adsplay.delivery.redis.cache;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import redis.clients.jedis.Pipeline;
import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.RedisConfigs;
import rfx.core.nosql.jedis.RedisCommand;
import rfx.core.util.LogUtil;

public class RealtimeTracking {

    // get redis pool in redis.config
    static ShardedJedisPool redisSampleData = RedisConfigs.load().get("creativeData").getShardedJedisPool();

    static ShardedJedisPool bookingDataPool = RedisConfigs.load().get("bookingData").getShardedJedisPool();

    static ShardedJedisPool reportDataPool = RedisConfigs.load().get("reportData").getShardedJedisPool();

    static ShardedJedisPool inventoryDataPool = RedisConfigs.load().get("inventoryData").getShardedJedisPool();

    static ShardedJedisPool placementDataPool = RedisConfigs.load().get("placementData").getShardedJedisPool();

    private static final String SUMMARY = "summary";

    public static final int ONE_DAY = 86400;

    public static final int AFTER_15_DAYS = ONE_DAY * 15;
    public static final int AFTER_30_DAYS = ONE_DAY * 30;
    public static final int AFTER_60_DAYS = ONE_DAY * 60;

    public static String get(final String key) {
        String result = new RedisCommand<String>(bookingDataPool) {
            @Override
            protected String build() throws JedisException {
                return jedis.get(key);
            }
        }.execute();
        return result;
    }

    public static String set(final String key, final String value) {
        String result = new RedisCommand<String>(bookingDataPool) {
            @Override
            protected String build() throws JedisException {
                try {
                    Pipeline p = jedis.pipelined();
                    p.set(key, value);
                    p.sync();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
        return result;
    }

    /**
     * 
     * @param prefix
     * @param unixtime
     * @param events
     * @param withSummary
     * @return
     */
    public static boolean updateEvent(final String[] events, boolean withSummary, TrackingRedisPool redisPool) {
        boolean commited = false;
        if (events.length == 0) {
            return false;
        }

        ShardedJedisPool redisPoolProxy = null;

        if (redisPool.equals(TrackingRedisPool.BOOKING)) {
            redisPoolProxy = bookingDataPool;
        } else if (redisPool.equals(TrackingRedisPool.REPORT_DATA)) {
            redisPoolProxy = reportDataPool;
        } else if (redisPool.equals(TrackingRedisPool.INV_DATA)) {
            redisPoolProxy = inventoryDataPool;
        } else if (redisPool.equals(TrackingRedisPool.PLACEMENT)) {
            redisPoolProxy = placementDataPool;
        }
        try {
            RedisCommand<Boolean> cmd = new RedisCommand<Boolean>(redisPoolProxy) {
                @Override
                protected Boolean build() throws JedisException {

                    Pipeline p = jedis.pipelined();
                    for (String event : events) {
                        try {
                            if (withSummary) {
                                p.incr(SUMMARY);
                            }
                            p.incr(event);
                            p.expire(event, AFTER_30_DAYS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    p.sync();
                    return true;
                }
            };
            if (cmd != null) {
                commited = cmd.execute();
            }
        } catch (Exception e) {
            LogUtil.error(RealtimeTracking.class, "updateEvent function", "Error when updateEvent to redis cache !!!");
            LogUtil.error(e);
        }
        return commited;
    }

    /**
     * 
     * @param prefix
     * @param unixtime
     * @param events
     * @param withSummary
     * @return
     */
    public static boolean updateHEvent(final Map<String, List<String>> mapEvents, TrackingRedisPool redisPool) {
        boolean commited = false;
        if (mapEvents.isEmpty()) {
            return false;
        }

        ShardedJedisPool redisPoolProxy = null;

        if (redisPool.equals(TrackingRedisPool.BOOKING)) {
            redisPoolProxy = bookingDataPool;
        } else if (redisPool.equals(TrackingRedisPool.REPORT_DATA)) {
            redisPoolProxy = reportDataPool;
        } else if (redisPool.equals(TrackingRedisPool.INV_DATA)) {
            redisPoolProxy = inventoryDataPool;
        } else if (redisPool.equals(TrackingRedisPool.PLACEMENT)) {
            redisPoolProxy = placementDataPool;
        }
        try {
            RedisCommand<Boolean> cmd = new RedisCommand<Boolean>(redisPoolProxy) {
                @Override
                protected Boolean build() throws JedisException {

                    Pipeline p = jedis.pipelined();
                    for (Entry<String, List<String>> event : mapEvents.entrySet()) {
                        String key = event.getKey();
                        List<String> fields = event.getValue();
                        fields.forEach(field -> {
                            p.hincrBy(key, field, 1L);
                            p.expire(key, AFTER_30_DAYS);
                        });
                    }
                    p.sync();
                    return true;
                }
            };
            if (cmd != null) {
                commited = cmd.execute();
            }
        } catch (Exception e) {
            LogUtil.error(RealtimeTracking.class, "updateHEvent function",
                    "Error when updateHEvent to redis cache !!!" + ", at " + redisPool);
            e.printStackTrace();
        }
        return commited;
    }

    public static void main(String[] args) {

    }
}
