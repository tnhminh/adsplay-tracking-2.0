package adsplay.thirdparty;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.annotations.Expose;

public class ThirdPartyTrackingData {
	@Expose
	List<String> impressionEventLinks ;
	
	@Expose
	List<String> completeEventLinks;
	
	@Expose
	List<String> clickEventLinks;
	
	@Expose(serialize=false)
	String vastXml;
	public ThirdPartyTrackingData(String vastXml) {
		this.vastXml = vastXml;
		parseData();
	}
	
	void parseData(){
		Document doc = Jsoup.parse(vastXml);
		
		Elements impNodes = doc.select("Impression");
		impressionEventLinks = new ArrayList<>(impNodes.size());
		for (Element node : impNodes) {
			impressionEventLinks.add(node.text());
		}
		
		Elements completeNodes = doc.select("Tracking[event=complete]");
		completeEventLinks = new ArrayList<String>(completeNodes.size());
		for (Element node : completeNodes) {
			completeEventLinks.add(node.text());
		}
		
		Elements clickTrackingNodes = doc.select("ClickTracking");
		clickEventLinks = new ArrayList<String>(clickTrackingNodes.size());
		for (Element node : completeNodes) {
			clickEventLinks.add(node.text());
		}
		
	}
	
	public void addImpressionEventLink(String link){
		impressionEventLinks.add(link);
	}
	
	public void addCompleteEventLink(String link){
		completeEventLinks.add(link);
	}
	
	public void addClickEventLinks(String link){
		clickEventLinks.add(link);
	}

	public List<String> getImpressionEventLinks() {
		return impressionEventLinks;
	}

	public List<String> getCompleteEventLinks() {
		return completeEventLinks;
	}

	public List<String> getClickEventLinks() {
		return clickEventLinks;
	}

	public String getVastXml() {
		return vastXml;
	}
	
	
}
