package adsplay.common;

import adsplay.data.Creative;

public class PlatformUtil {
	static String DEVICE_TYPE_PC = "General_Desktop";
	static String DEVICE_TYPE_MOBILE_WEB = "General_Mobile";
	static String DEVICE_TYPE_TABLET = "General_Tablet";
	
	
	public static int getPlatformId(int placement){
		int platformId = Creative.PLATFORM_PC;	
		if((placement>200 && placement <299) || placement> 1000){
        	platformId = Creative.PLATFORM_SMART_TV;
        } else if(placement>300 && placement <400){
        	platformId = Creative.PLATFORM_NATIVE_APP;
        } 
	    return platformId;
	}
	
	
}
