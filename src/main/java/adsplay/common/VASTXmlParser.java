package adsplay.common;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import adsplay.common.template.TemplateUtil;
import adsplay.server.delivery.video.model.AdTrackingUrl;
import rfx.core.util.DateTimeUtil;
import rfx.core.util.HttpClientUtil;
import rfx.core.util.RandomUtil;
import rfx.core.util.StringUtil;

public class VASTXmlParser {

  public static final String VAST_EVENT_COMPLETE = "complete";
  public static final String VAST_EVENT_IMPRESSION = "impression";
  private static final String HTTPS = "https:";
  private static final String HTTP = "http:";

  public static class VastXmlData {
    StringBuilder eventTrackingTags = new StringBuilder();
    StringBuilder clickTags = new StringBuilder();

    List<String> impression3rdUrls = new ArrayList<>();
    List<AdTrackingUrl> streamAdTrackingUrls = new ArrayList<>();

    public void addImpression3rdUrl(String tag) {
      impression3rdUrls.add(tag);
    }

    public void addEventTrackingTag(String tag) {
      eventTrackingTags.append(tag);
    }

    public void addClickTag(String tag) {
      clickTags.append(tag);
    }

    public List<String> getImpression3rdUrls() {
      return impression3rdUrls;
    }

    public String getEventTrackingTags() {
      return eventTrackingTags.toString();
    }

    public String getClickTags() {
      return clickTags.toString();
    }

    public void addStreamAdTrackingUrl(String event, String url, int delay) {
      this.streamAdTrackingUrls.add(new AdTrackingUrl(event, url, delay));
    }

    public List<AdTrackingUrl> getStreamAdTrackingUrls() {
      return streamAdTrackingUrls;
    }
  }

  public static String beginImgTag = "<Impression>";
  public static String endImgTag = "</Impression>";
  public static String beginTrackingEventTag = "<TrackingEvents>";
  public static String endTrackingEventTag = "</TrackingEvents>";
  public static String beginVideoClicksTag = "<VideoClicks>";
  public static String endVideoClicksTag = "</VideoClicks>";

  public static final String NO_AD_XML = TemplateUtil.render("delivery-ads/no-ads-vast2.xml");
  public static final String NO_AD_XML_VMAP =
      "<?xml version=\"1.0\" encoding=\"UTF-8\"?><vmap:VMAP xmlns:vmap=\"http://www.iab.net/videosuite/vmap\" version=\"1.0\"/>";

  static void process(Document doc, VastXmlData vastXmlData) {
    int time = DateTimeUtil.currentUnixTimestamp() + RandomUtil.getRandom(10000);

    //// vastXml = xml.replace("http://visitanalytics", "https://visitanalytics");
    Elements impNodes = doc.select("Impression");
    for (Element node : impNodes) {
      String url = node.text().replace(HTTP, HTTPS).replace("[timestamp]", time + "");

      // System.out.println(url);
      vastXmlData.addImpression3rdUrl(url);
      vastXmlData.addStreamAdTrackingUrl(VAST_EVENT_IMPRESSION, url, 1);
    }

    System.out.println("------------------");
    Elements eventNodes = doc.select("Tracking[event]");

    for (Element node : eventNodes) {
      String trackingUrl = node.attr("event");
      String event = trackingUrl.toLowerCase();

      String eventValue = node.text();
      if (StringUtil.isNotEmpty(eventValue) && StringUtil.isNotEmpty(event)) {
        if (event.equalsIgnoreCase("start")) {
          event = "creativeview";
        }
        String url = node.text().replace(HTTP, HTTPS).replace("[timestamp]", time + "");
        String tag = "<Tracking event=\"" + trackingUrl + "\"><![CDATA[" + url + "]]></Tracking>\n";
        // System.out.println(tag);
        vastXmlData.addEventTrackingTag(tag);

        if (event.equals("creativeview")) {
          vastXmlData.addStreamAdTrackingUrl(trackingUrl, url, 3);
        } else if (event.equals("firstquartile")) {
          vastXmlData.addStreamAdTrackingUrl(trackingUrl, url, 10);
        } else if (event.equals("midpoint")) {
          vastXmlData.addStreamAdTrackingUrl(trackingUrl, url, 20);
        } else if (event.equals("thirdquartile")) {
          vastXmlData.addStreamAdTrackingUrl(trackingUrl, url, 30);
        } else if (event.equals(VAST_EVENT_COMPLETE)) {
          vastXmlData.addStreamAdTrackingUrl(trackingUrl, url, 40);
        }
      }
    }

    System.out.println("------------------");
    Elements clickTrackingNodes = doc.select("ClickTracking");
    for (Element node : clickTrackingNodes) {
      String url = node.text().replace(HTTP, HTTPS) + "&_t=" + time;
      String tag = "<ClickTracking><![CDATA[" + url + "]]></ClickTracking>";
      System.out.println(tag);
      vastXmlData.addClickTag(tag);
    }

    System.out.println("------------------");
    Elements clickThroughNodes = doc.select("ClickThrough");
    for (Element node : clickThroughNodes) {
      String url = node.text();
      if (url.contains("track.adnetwork.vn")) {
        String tag = "<ClickTracking><![CDATA[" + url.replace(HTTP, HTTPS) + "]]></ClickTracking>";
        vastXmlData.addClickTag(tag);
      } else {
        String tag = "<ClickThrough><![CDATA[" + url + "]]></ClickThrough>";
        vastXmlData.addClickTag(tag);
      }
    }
  }

  static LoadingCache<String, String> xml3rdDataCache =
      CacheBuilder.newBuilder().maximumSize(100000).expireAfterWrite(2800, TimeUnit.MILLISECONDS)
          .build(new CacheLoader<String, String>() {
            public String load(String url) {
              String xml = HttpClientUtil.executeGet(url);
              return xml;
            }
          });

  public static VastXmlData parse(String xml) {
    VastXmlData vastXmlData = new VastXmlData();
    Document doc = Jsoup.parse(xml);

    System.out.println("-----BEGIN:VASTXmlParser------");
    System.out.println(xml);
    System.out.println("-----END:VASTXmlParser------");

    Elements vastTagURINodes = doc.select("VASTAdTagURI");
    String vASTAdTagURI = null;
    for (Element node : vastTagURINodes) {
      vASTAdTagURI = node.text();
    }

    process(doc, vastXmlData);

    if (vASTAdTagURI != null) {
      String xml2;
      try {
        xml2 = xml3rdDataCache.get(vASTAdTagURI);
        Document doc2 = Jsoup.parse(xml2);
        process(doc2, vastXmlData);
      } catch (Exception e) {
      }
    }

    return vastXmlData;
  }

  public static void main(String[] args) {
    String url = "http://direct.adsrvr.org/bid/bidder/publisherdirect/?video.inline=1&maxdur=30&did=ttdd-1tqkzuh-ivzlru2&ord=[timestamp]";
    String xml = HttpClientUtil.executeGet(url);
    VastXmlData data = parse(xml);

    System.out.println("VastXmlData.getImpressionTags " + data.getImpression3rdUrls());
    System.out.println("VastXmlData.getEventTrackingTags " + data.getEventTrackingTags());
    System.out.println("VastXmlData.getClickTags " + data.getClickTags());
    System.out.println("VastXmlData.getAdsStream " + data.getStreamAdTrackingUrls());

  }
}
