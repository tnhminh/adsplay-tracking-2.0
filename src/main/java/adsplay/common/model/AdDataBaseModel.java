package adsplay.common.model;

import adsplay.common.AdBeaconUtil;

/**
 * @author trieu
 * 
 * base model for special ad data format (mobile, infeed, inpage, masterhead, 360, lightbox) 
 *
 */
public abstract class AdDataBaseModel {

	protected String adMedia;//Video Ad URL or Banner Ad URL
	protected String clickthroughUrl;//	Call to action destination URL
	protected String clickActionText;//Call to action text
	protected int adId;
	protected String adBeacon;
	protected int adType = 0;
	protected int placementId = 0;
	protected String adCode = "";//3rd JavaScript code

	public AdDataBaseModel() {
		super();
	}

	public String getAdText() {
		return adMedia;
	}

	public String getClickthroughUrl() {
		return clickthroughUrl;
	}

	public String getClickActionText() {
		return clickActionText;
	}

	public int getAdId() {
		return adId;
	}

	public String getAdBeacon() {
		return adBeacon;
	}

	public void setAdText(String adText) {
		this.adMedia = adText;
	}

	public void setClickthroughUrl(String clickthroughUrl) {
		this.clickthroughUrl = clickthroughUrl;
	}

	public void setClickActionText(String clickActionText) {
		this.clickActionText = clickActionText;
	}

	public void setAdId(int adId) {
		this.adId = adId;
	}

	public int getAdType() {
		return adType;
	}

	public String getAdCode() {
		return adCode;
	}

	public void setAdCode(String adCode) {
		this.adCode = adCode;
	}

	public String getAdMedia() {
		return adMedia;
	}

	public void setAdMedia(String adMedia) {
		this.adMedia = adMedia;
	}

	public int getPlacementId() {
		return placementId;
	}

	public void setPlacementId(int placementId) {
		this.placementId = placementId;
	}

	public void setAdType(int adType) {
		this.adType = adType;
	}

	public AdDataBaseModel buildBeaconData(String uuid, int campaignId, int flightId) {
		this.adBeacon = AdBeaconUtil.createBeaconValue(getAdId(), uuid, this.placementId, campaignId, flightId);
		return this;
	}

}