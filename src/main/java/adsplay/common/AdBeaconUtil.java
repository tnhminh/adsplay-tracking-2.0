package adsplay.common;

import rfx.core.util.DateTimeUtil;
import rfx.core.util.SecurityUtil;
import rfx.core.util.StringUtil;

public class AdBeaconUtil {

    public static String createBeaconValue(int adid, String uuid, int placementId, int campaignId, int flightId) {
        String rawBeacon = StringUtil.toString(uuid, "|", placementId, "|", adid, "|", campaignId, "|", flightId);
        return SecurityUtil.encryptBeaconValue(rawBeacon);
    }

    public static String getAdParams(int adid, String uuid, int placementId, int campaignId, int flightId) {
        StringBuilder adData = new StringBuilder();
        adData.append("&adid=").append(adid);
        adData.append("&flightId=").append(flightId);
        adData.append("&placementId=").append(placementId);
        adData.append("&t=").append(DateTimeUtil.currentUnixTimestamp());
        return adData.toString();
    }
}
