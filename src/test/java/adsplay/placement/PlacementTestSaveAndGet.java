package adsplay.placement;

import adsplay.api.dao.PlacementMongoDbDao;
import adsplay.data.Placement;
import adsplay.server.delivery.query.PlacementQueryUtil;

public class PlacementTestSaveAndGet {

	public static void main(String[] args) {
		String adCode =  "<script async src='//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js'></script>"
		 + "<ins class='adsbygoogle' style='display:inline-block;width:728px;height:90px' data-ad-client='ca-pub-1248807399534974' data-ad-slot='9229775046'></ins>"
		 + "<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>";
		
		Placement placement = new Placement("adsplay-demo-300x250", "adsplay", Placement.PLACEMENT_DISPLAY, 300, 250, "adsplay.net");
		placement.setAdCode3rd(adCode);
		int id = PlacementMongoDbDao.save(placement);
		System.out.println(id);
		
		for (int i = 0; i < 10; i++) {
			Placement pm = PlacementQueryUtil.get(id);
			System.out.println(pm.getId().equals(id));	
		}
		
//		int placementId = 1000;
//		String uuid = "123";
//		BannerAdData ad = null;
//		String adMedia = "https://st.adsplay.net/ads/banner/189.gif";					
//		String clickthroughUrl = "http://logging.adtop.vn/tracking/click/1536/110/565/9624/0/0/1/1/0/4401a14288d1fea37d8c354786d6ffeb1fc145c09f14bfebdf471b722a4d2390";
//		String clickActionText = "Game SoaiVuong";
//		int adId = 190;
//		int adType = Creative.ADTYPE_IMAGE_DISPLAY_AD;
//		ad = new BannerAdData(placementId,adMedia, clickthroughUrl, clickActionText, adId, adType,728,90);		
//		ad.buildBeaconData(uuid, placementId, 1000 + adId, 10000 + adId);
	}
	
	
}
