package adsplay.dao.test;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import redis.clients.jedis.ShardedJedisPool;
import redis.clients.jedis.exceptions.JedisException;
import rfx.core.configs.RedisConfigs;
import rfx.core.nosql.jedis.RedisCommand;
import rfx.core.util.StringUtil;

public class DumpLocationData {
	
	static ShardedJedisPool jedisPool = RedisConfigs.load().get("locationDataStats").getShardedJedisPool();
	
	static Map<String,Boolean> northCities = new HashMap<>();
	static Map<String,Boolean> southCities = new HashMap<>();
	static {
		southCities.put("THANH PHO HO CHI MINH", true);
		southCities.put("BIEN HOA", true);
		southCities.put("CAN THO", true);
		southCities.put("THU DAU MOT", true);
		southCities.put("VUNG TAU", true);
		southCities.put("CA MAU", true);
		southCities.put("CAN THO", true);
		southCities.put("NHA TRANG", true);
		southCities.put("LONG XUYEN", true);
		southCities.put("DA LAT", true);
	}
		
	static class GeoLocationDataStats {
		String country;
		String city;
		String region;
		double lat,lon;
		String area;
		long count;
		
		public GeoLocationDataStats(String country, String city, String region, double lat, double lon, String area, long count) {
			super();
			this.country = country;
			this.city = city;
			this.region = region;
			this.lat = lat;
			this.lon = lon;
			this.area = area;
			this.count = count;
		}
		public String getCountry() {
			return country;
		}
		public String getCity() {
			return city;
		}
		public String getRegion() {
			return region;
		}
		public double getLat() {
			return lat;
		}
		public double getLon() {
			return lon;
		}
		public String getArea() {
			if(!String.valueOf(area).startsWith("vn-")){
				if("THANH PHO HO CHI MINH".equals(city) ){
					area = "vn-south";
				}
				else if("HA NOI".equals(city) || "HUE".equals(city)){
					area = "vn-north";
				} else {
					
				}
			}
			return area;
		}
		public void setCountry(String country) {
			this.country = country;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public void setRegion(String region) {
			this.region = region;
		}
		public void setLat(double lat) {
			this.lat = lat;
		}
		public void setLon(double lon) {
			this.lon = lon;
		}
		public void setArea(String area) {
			this.area = area;
		}	
		
		public long getCount() {
			return count;
		}
		public void setCount(long count) {
			this.count = count;
		}
		@Override
		public String toString() {
			int _____ = 1;
			return new Gson().toJson(this);
		}
		
	}
	
	public static void main(String[] args) {
		
//		String ip = "118.69.131.254";
//		String regionFromIp = Ip2LocationUtil.findCityAndUpdateStats(ip,RealtimeTrackingUtil.LOCATION_METRIC_PLAYVIEW,null);
//		System.out.println(regionFromIp);
		
		Map<String, Long> map = new HashMap<>();	
		AtomicLong countNorth = new AtomicLong(0);
		AtomicLong countSouth = new AtomicLong(0);
		
		List<GeoLocationDataStats> locStats = new RedisCommand<Set<String>>(jedisPool) {
			@Override
			protected Set<String> build() throws JedisException {					
				Set<String> locs = jedis.keys("lc:*");				
				return locs;
			}
		}.execute().stream().map((String key)->{
			return new RedisCommand<GeoLocationDataStats>(jedisPool) {
				@Override
				protected GeoLocationDataStats build() throws JedisException {					
					String locDetails = jedis.get(key);
					if(locDetails.toLowerCase().startsWith("viet")){
						long count = StringUtil.safeParseLong(jedis.get(key.replace("lc:", "lcs:")));
//						System.out.println(key + " => " + count);
						
						String[] toks = locDetails.split("#");
						if(toks.length >=6){
							double lat = StringUtil.safeParseDouble(toks[2]);
							double lon= StringUtil.safeParseDouble(toks[3]);
							GeoLocationDataStats locStat = new GeoLocationDataStats(toks[0], toks[1], toks[4], lat, lon, toks[5], count);
							if("vn-north".equals(locStat.getArea())){
								countNorth.addAndGet(count);
							}
							if("vn-south".equals(locStat.getArea())){
								countSouth.addAndGet(count);
							}
							return locStat;
						}						
					}
					return null;
				}
			}.execute();
		}).filter(locStat -> {
			//System.out.println(loc);
			return locStat != null;
			//return true;
		}).collect(Collectors.toList());
		
		Collections.sort(locStats, new Comparator<GeoLocationDataStats>(){
			@Override
			public int compare(GeoLocationDataStats o1, GeoLocationDataStats o2) {
				// TODO Auto-generated method stub
				if(o1.getCount()>o2.getCount()){
					return -1;
				} else if(o1.getCount()<o2.getCount()){
					return 1;
				}
				return 0;
			}			
		});
	
		if(locStats.size()>20){
			locStats = locStats.subList(0, 50);			
		}
		
		
		for (GeoLocationDataStats locStat : locStats) {			
			System.out.println(locStat);
		}
		System.out.println("countNorth "+ countNorth.get());
		System.out.println("countSouth "+ countSouth.get());
		
		
	}
	
	

}
