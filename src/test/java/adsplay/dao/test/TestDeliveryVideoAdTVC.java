package adsplay.dao.test;

import static com.rabriel.GivenWhenThen.given;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

import org.junit.Test;

public class TestDeliveryVideoAdTVC {

	@Test
	public void basicFlowTest() {
		given(1)
		.when("multiplying by 2", givenValue -> 2 * givenValue)
		.then("value should be even",
				whenValue -> whenValue % 2 == 0);
	}

	@Test
	public void multiTypeFlowTest() {
		LocalDateTime localDateTime = LocalDateTime.now();
		DayOfWeek expectedDay = localDateTime.getDayOfWeek();

		given(localDateTime).when("retrieving current day", date -> date.getDayOfWeek()).then("days should match",
				day -> expectedDay == day);
	}

}
