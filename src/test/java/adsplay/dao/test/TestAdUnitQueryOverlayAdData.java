package adsplay.dao.test;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import adsplay.common.AdTimeUtil;
import adsplay.data.Creative;
import adsplay.server.delivery.query.AdUnitQueryUtil;
import adsplay.server.delivery.query.Query;

public class TestAdUnitQueryOverlayAdData {
	static Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	@Test
	public void testQueryImageOverlay(){
		boolean peakTime = AdTimeUtil.isPeakTime();
		int platformId = Creative.PLATFORM_PC;
		int placementId = 102;
		String uuid = "abcd";
		String location = "vn-south";
		String cityFromIp = "HA NOI";
		
		Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
//		q.setMaleGroupOnVOD(UserSegmentUtil.isMaleGroupOnVOD(uuid, cxkw, refererHeaderUrl, refererUrl));
//		q.setFemaleGroupOnVOD(UserSegmentUtil.isFemaleGroupOnVOD(uuid, cxkw, refererHeaderUrl, refererUrl));
		q.setPeakTime(peakTime);
		q.setLiveTV(false);
		q.setAdType(Creative.ADTYPE_IMAGE_OVERLAY);
		int autoHideTimeout = 9000;
		int timeToShow = 100000;
		
//		System.out.println(gson.toJson(AdUnitQueryUtil.queryVideoAd(q).getInstreamVideoAds()) );
		
		System.out.println(gson.toJson(AdUnitQueryUtil.queryOverlayAdData(q, autoHideTimeout, timeToShow)));
	}
	
	@Test
	public void testQueryBreakingNewsOverlay(){
		boolean peakTime = AdTimeUtil.isPeakTime();
		int platformId = Creative.PLATFORM_PC;
		int placementId = 102;
		String uuid = "abcd";
		String location = "vn-north";
		String cityFromIp = "HA NOI";
		
		Query q = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
//		q.setMaleGroupOnVOD(UserSegmentUtil.isMaleGroupOnVOD(uuid, cxkw, refererHeaderUrl, refererUrl));
//		q.setFemaleGroupOnVOD(UserSegmentUtil.isFemaleGroupOnVOD(uuid, cxkw, refererHeaderUrl, refererUrl));
		q.setPeakTime(peakTime);
		q.setLiveTV(false);
		q.setAdType(Creative.ADTYPE_BREAKING_NEWS_OVERLAY);
		
		int autoHideTimeout = 9000;
		int timeToShow = 100000;
		
//		System.out.println(gson.toJson(AdUnitQueryUtil.queryVideoAd(q).getInstreamVideoAds()) );
		
		System.out.println(gson.toJson(AdUnitQueryUtil.queryOverlayAdData(q, autoHideTimeout, timeToShow)));
	}
}
