package adsplay.bidding.xml;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import adsplay.thirdparty.ThirdPartyTrackingData;
import rfx.core.util.HttpClientUtil;
import rfx.core.util.RandomUtil;
import rfx.core.util.Utils;

public class ParsingVastXml {
	
	private static final String URL_TARGET_MALE_WEB = "http://adsplay.net/demo-v2/1429.xml";
	static LoadingCache<String, String> xml3rdDataCache = CacheBuilder.newBuilder()
			.maximumSize(10).expireAfterWrite(5000, TimeUnit.MILLISECONDS)
			.build(new CacheLoader<String, String>() {
				public String load(String url) {								
					return HttpClientUtil.executeGet(url);
				}
			});
	

	static void test() throws ExecutionException{
		String xml = xml3rdDataCache.get(URL_TARGET_MALE_WEB);
		//System.out.println(data);
		
		Document doc = Jsoup.parse(xml);
		System.out.println("------------------");
		Elements impNodes = doc.select("Impression");
		for (Element node : impNodes) {
			String tag = "<Impression><![CDATA[" + node.text() + "]]></Impression>";
			System.out.println(tag);
		}
		
		System.out.println("------------------");
		Elements completeNodes = doc.select("Tracking[event=complete]");
		for (Element node : completeNodes) {
			String tag = "<Impression><![CDATA[" + node.text() + "]]></Impression>";
			System.out.println(tag);
		}
		
		System.out.println("------------------");
		Elements creativeViewNodes = doc.select("Tracking[event=creativeView]");
		for (Element node : creativeViewNodes) {
			String tag = "<Tracking event=\"creativeView\"><![CDATA[" + node.text() + "]]></Tracking>";
			System.out.println(tag);
		}
		
		System.out.println("------------------");
		Elements clickTrackingNodes = doc.select("ClickTracking");
		for (Element node : clickTrackingNodes) {
			String tag = "<ClickTracking><![CDATA[" + node.text() + "]]></ClickTracking>";
			System.out.println(tag);
		}
		
		System.out.println("------------------");
		Elements mediaFileNodes = doc.select("MediaFile");
		for (Element node : mediaFileNodes) {
			System.out.println(node.text());
		}
	}
	
	static void test2() throws ExecutionException{
		String data = xml3rdDataCache.get(URL_TARGET_MALE_WEB);
		ThirdPartyTrackingData trackingData = new ThirdPartyTrackingData(data);
		trackingData.addImpressionEventLink("http://aaa");
		System.out.println(trackingData.getImpressionEventLinks());
	}
	

	public static void main(String[] args) throws ExecutionException {
		
		long b = System.currentTimeMillis();
		
		for (int i = 0; i < 1; i++) {
			test();
			Utils.sleep(RandomUtil.getRandom(200));
		}
		
		System.out.println(System.currentTimeMillis() - b);
		
	}
}
