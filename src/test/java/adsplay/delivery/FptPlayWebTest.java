package adsplay.delivery;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Test;

import rfx.core.util.HttpClientUtil;
import rfx.core.util.StringUtil;

public class FptPlayWebTest {

	@Test
	public void test() {
		String url = "http://d4.adsplay.net/delivery/zone/1001?uuid=0011eb68b0d34ab085a9d0160b70d943&placement=102&referrer=https%3A%2F%2Ffptplay.net%2Fdanh-muc%2F52842df7169a580a79169efd%2Fthe-thao.html&url=https%3A%2F%2Ffptplay.net%2Fxem-video%2Fjavier-hernandez-va-man-trinh-dien-an-tuong-tai-bayer-leverkusen-bundesliga-2015-2016-5707157d17dc1359dbfd860a.html&cxt=FPT%20Play%20-%20Xem%20video%20Javier%20Hernandez%20v%C3%A0%20m%C3%A0n%20tr%C3%ACnh%20di%E1%BB%85n%20%E1%BA%A5n%20t%C6%B0%E1%BB%A3ng%20t%E1%BA%A1i%20Bayer%20Leverkusen%20-%20Bundesliga%202015-2016&cxkw=B%C3%B3ng%20%C4%90%C3%A1%20Qu%E1%BB%91c%20T%E1%BA%BF&ut=0&t=1460085436246&at=tvc";
		String vastXml = HttpClientUtil.executeGet(url);
		
		Document doc = Jsoup.parse(vastXml);
		
		Elements impNodes = doc.select("Impression");
		Elements mediaFileNodes = doc.select("MediaFile");
				
		boolean actual = impNodes.size()> 0 && mediaFileNodes.size()> 0;
		boolean expected = true;
		Assert.assertEquals(expected, actual);
		
		String mediaFile = mediaFileNodes.get(0).text();
		Assert.assertEquals(expected, StringUtil.isNotEmpty(mediaFile));
		
		System.out.println(mediaFile);
	}

}