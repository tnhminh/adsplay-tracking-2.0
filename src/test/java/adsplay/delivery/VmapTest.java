package adsplay.delivery;

import adsplay.common.template.TemplateUtil;
import adsplay.server.delivery.query.InstreamAdDataUtil;
import adsplay.server.delivery.video.model.AdBreak;
import adsplay.server.delivery.video.model.VmapDataModel;

public class VmapTest {

	public static void main(String[] args) {
		VmapDataModel model = new VmapDataModel();
		model.addAdBreak(new AdBreak("start", "https://d4.adsplay.net/delivery/zone/1001?placement=302"));
		model.addAdBreak(new AdBreak("00:00:22", "https://d4.adsplay.net/delivery/zone/1001?placement=302"));		
		String xml = TemplateUtil.render(InstreamAdDataUtil.VMAP_NATIVE_MOBILE, model);
		System.out.println(xml);
		
	}
}
