package adsplay.delivery;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import adsplay.common.template.TemplateUtil;
import adsplay.data.Creative;
import adsplay.server.delivery.query.AdUnitQueryUtil;
import adsplay.server.delivery.query.InstreamAdDataUtil;
import adsplay.server.delivery.query.Query;
import adsplay.server.delivery.video.model.InstreamVideoAd;
import adsplay.server.delivery.video.model.VideoAdDataModel;
import adsplay.targeting.LocationDataUtil;
import rfx.core.util.Utils;

public class MongoDbQueryTest {
	
	static Query fptplayWebVodQuery;
	static {
		int platformId = 1;
		int placementId = 102;
		String uuid = "0011eb68b0d34ab085a9d0160b70d943";
		String location = "";
		String region = "dac lac";
		fptplayWebVodQuery = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
		AdUnitQueryUtil.initThirdpartyUrls();
	}	
	
	public void testWithDelay() {		
		List<Creative> creatives = AdUnitQueryUtil.queryFromMongoDb(fptplayWebVodQuery);
		Utils.sleep(500);
		
		creatives = AdUnitQueryUtil.queryFromMongoDb(fptplayWebVodQuery);
		Utils.sleep(5000);
		creatives = AdUnitQueryUtil.queryFromMongoDb(fptplayWebVodQuery);
				
		boolean actual = creatives.size()> 0 ;		
		boolean expected = true;
		Assert.assertEquals(expected, actual);
		
		for (Creative creative : creatives) {
			System.out.println(creative.getId()+" => "+creative);
		}
	}

	
	public void simpleTestQuery() {		
		List<Creative> creatives = AdUnitQueryUtil.queryFromMongoDb(fptplayWebVodQuery);		
				
		boolean actual = creatives.size()> 0 ;		
		boolean expected = true;
		Assert.assertEquals(expected, actual);
		
		for (Creative creative : creatives) {
			System.out.println(creative.getId()+" => "+creative);
		}
	}
	
	@Test
	public void testQueryDataModel(){		
		int numQuery = 3;
		for (int i = 0; i < numQuery; i++) {
			Utils.sleep(3000);
			int platformId = Creative.PLATFORM_PC;
			int placementId = 101;
			String uuid = "abcd";			
			Query q = new Query(platformId, placementId, uuid , Query.CONTENT_GROUP_VOD );
//			q.setMaleGroupOnVOD(true);
//			q.setFemaleGroupOnVOD(false);
//			q.setPeakTime(peakTime);
//			q.setLiveTV(false);
//			VideoAdDataModel model = AdUnitQueryUtil.querySingleAd(q);
			
			VideoAdDataModel model = AdUnitQueryUtil.queryByAdId(1177, platformId, placementId, uuid,"");
			Assert.assertEquals(true, model != null);
//			Assert.assertEquals(true, model.getInstreamVideoAds().size()>0);
			
			for (InstreamVideoAd instreamVideoAd : model.getInstreamVideoAds()) {
				System.out.println(instreamVideoAd.getAdTitle() + " " + instreamVideoAd.getStartTime() );
								
				String xml = TemplateUtil.render(InstreamAdDataUtil.VAST3_NATIVE_MOBILE, model);
				System.out.println("--- VastXML: \n");
				System.out.println(xml);
			}			
		}	
	}
	
	
	public void queryPayTVads() {
		int platformId = 6;
		int placementId = 320;
		String uuid = "";
		String location = LocationDataUtil.VN_SOUTH;
		String city = "HO CHI MINH";
		Query query = new Query(platformId, placementId, uuid, Query.CONTENT_GROUP_VOD, location);
		List<Creative> creatives = AdUnitQueryUtil.queryFromMongoDb(query);	
				
		for (Creative creative : creatives) {
			System.out.println(creative.getId() +" => "+creative);
		}
	}

}
