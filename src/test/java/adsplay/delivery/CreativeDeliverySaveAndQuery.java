package adsplay.delivery;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

import adsplay.api.dao.CreativeMongoDbDao;
import adsplay.data.Creative;
import adsplay.server.delivery.query.MongoAdQuery;
import adsplay.targeting.LocationDataUtil;
import rfx.core.util.DateTimeUtil;

public class CreativeDeliverySaveAndQuery {

    static MongoClient mongoClient = new MongoClient( "localhost", 27017 );
    static final Morphia morphia = new Morphia();
    static {
        morphia.mapPackage("adsplay.data");
    }

    public static void main(String[] args) throws ParseException {        
    	int adType = Creative.ADTYPE_INSTREAM_VIDEO;
        DateFormat dateFormat = new SimpleDateFormat(DateTimeUtil.DATE_HOUR_FORMAT_PATTERN);

        // init sample data CreativeHourlyStats
        Creative crt1 = new Creative(168, "iTVad-Hyundai-trial-test-2016", "168-sd.mp4", "itvad", dateFormat.parse("2016-03-30-18"), dateFormat.parse("2016-03-30-18"), dateFormat.parse("2017-06-03-18"));
        crt1.setClickThrough("http://www.hyundai-thanhcong.vn/san-pham/tucson?utm_source=iTVad&utm_medium=Preroll&utm_campaign=Tuscon");
        crt1.setSkipoffset("00:00:05");
        crt1.setDuration("00:00:29");
        crt1.setAdType(adType);
        crt1.setCampaignId(10168);
        crt1.setFlightId(1168);        
        crt1.setPricingModel(Creative.PRICING_MODEL_CPM);
        crt1.setStatus(Creative.ADSTATUS_RUNNING);
        crt1.setTotalBooking(200000); 
        crt1.setScore(30);
        //set targeting fields        
        crt1.setTargetedGenders(Arrays.asList(Creative.GENDER_TARGET_ALL));
        crt1.setTargetedLocations(Arrays.asList("ho chi minh", "ha noi"));
        crt1.setTargetedKeywords(Arrays.asList("phim le","the thao"));
        crt1.setTargetedPlacements(Arrays.asList(101,102,201));
        crt1.setTargetedPlatforms(Arrays.asList(Creative.PLATFORM_PC,Creative.PLATFORM_SMART_TV, Creative.PLATFORM_NATIVE_APP));        
        CreativeMongoDbDao.saveCreative(crt1);   
        
        Creative crt2 = new Creative(180, "iTVad-TVC-Test-PhuTaiLand", "168-sd.mp4", "itvad", dateFormat.parse("2016-03-30-18"), dateFormat.parse("2016-03-30-18"), dateFormat.parse("2017-06-03-18"));
        crt2.setClickThrough("http://phutailand.vn/Chung-cu/Chung-cu-Mon-City-My-Dinh-i862.html");
        crt2.setSkipoffset("00:00:05");
        crt2.setDuration("00:00:15");
        crt2.setCampaignId(10180);
        crt1.setAdType(adType);
        crt2.setFlightId(1180);        
        crt2.setPricingModel(Creative.PRICING_MODEL_CPM);
        crt2.setStatus(Creative.ADSTATUS_RUNNING);
        crt2.setTotalBooking(200000);  
        crt2.setScore(10);
        //set targeting fields        
        crt2.setTargetedGenders(Arrays.asList(Creative.GENDER_TARGET_ALL));       
        crt2.setTargetedKeywords(Arrays.asList("phim bo","tv show"));
        crt2.setTargetedPlacements(Arrays.asList(101,102,201));
        crt2.setTargetedPlatforms(Arrays.asList(Creative.PLATFORM_PC,Creative.PLATFORM_SMART_TV, Creative.PLATFORM_NATIVE_APP));        
        CreativeMongoDbDao.saveCreative(crt2); 
        
        
        //Test query        
		List<Integer> targetedPlatforms = Arrays.asList(Creative.PLATFORM_PC);
		List<Integer> targetedPlacements = Arrays.asList(101);
		List<Integer> targetedGenders = Arrays.asList(Creative.GENDER_TARGET_MALE);
		List<String> locs = Arrays.asList(LocationDataUtil.VN_SOUTH);		
		//List<Long> queriedKeywordIds = StreamUtil.toCRC64List(Arrays.asList("phim bo"));
		
		
		Creative finalCreative = null;
		List<Creative> matchedCreatives = MongoAdQuery.findCreativesByTargeting(adType, targetedPlacements, targetedPlatforms,locs,targetedGenders);
//		List<Creative> matchedCreatives = MongoAdQuery.findCreativesByTargeting(adType, targetedPlacements, targetedPlatforms);
		
		System.out.println("matchedCreatives " + matchedCreatives.size());
		for (Creative creative : matchedCreatives) {
			System.out.println(creative.getName() + " " + creative.getMedia());
		}
		
		List<Creative> highPriorityCreatives = matchedCreatives.stream().filter(crt -> { return crt.getScore() > 50 ;}).collect(Collectors.toList());
		long seed = System.nanoTime();
		if(highPriorityCreatives.size()>0){
			Collections.shuffle(highPriorityCreatives, new Random(seed));
			finalCreative = highPriorityCreatives.get(0);
		} else if(matchedCreatives.size()>0){
			Collections.shuffle(matchedCreatives, new Random(seed));
			finalCreative = matchedCreatives.get(0);
		}
		
		System.out.println(finalCreative);
    }

}
